DROP DATABASE if exists DTCDatabase;

CREATE DATABASE DTCDatabase;

USE DTCDatabase;

CREATE TABLE Users (
	userID int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
	username varchar(30) NOT NULL,
	pass varchar(33) NOT NULL,
	email varchar(40) NOT NULL,
	fname varchar(30) NOT NULL,
	lname varchar(30) NOT NULL,
	phoneNumber varchar(10) NOT NULL,
	-- 0 - Offline
	-- 1 - Online
	-- 2 - Away
	dtcstatus tinyint(4) NOT NULL DEFAULT 0,
	trynastatus varchar(30) DEFAULT ''
);

-- Totally guessing this part
CREATE TABLE FriendsList (
	friendID int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
	fromID int(11) NOT NULL,
	toID int(11) NOT NULL,
	pending tinyint(4) NOT NULL DEFAULT '0',
	FOREIGN KEY fk1(fromID) REFERENCES Users(userID),
	FOREIGN KEY fk2(toID) REFERENCES Users(userID)
);

-- Guessing this part too
CREATE TABLE Plan (
	planID int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
    creatorID int(11) NOT NULL,
	plantime varchar(20) NOT NULL,
	title varchar(15),
	description varchar(30),
	address varchar(50)
);

CREATE TABLE PlanUsers (
	planeUserID int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
	planID int(11) NOT NULL,
	userID int(11) NOT NULL,
	accepted boolean(1) NOT NULL,
	creator boolean(1) NOT NULL,
	
	FOREIGN KEY fk3 (planID) REFERENCES Plan(planID)
);

CREATE TABLE FriendRequests (
	friendRequestID int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
	fromUserID int(11) NOT NULL,
	toUserID int(11) NOT NULL,

	FOREIGN KEY fk4 (fromUserID) REFERENCES Users(userID),
	FOREIGN KEY fk5 (toUserID) REFERENCES Users(userID)
);

CREATE TABLE ChatMessages (
	chatMessageID int(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
	userID int(11) NOT NULL,
	planID int(11) NOT NULL,
	message varchar(144) NOT NULL,
    pending boolean NOT NULL,
	FOREIGN KEY fk6 (userID) REFERENCES Users (userID),
	FOREIGN KEY fk7 (planID) REFERENCES Plan (planID)
);