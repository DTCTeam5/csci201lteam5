package client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class UnregStartPanel extends JPanel {
	
	private static final long serialVersionUID = -3873772948494471727L;
	private boolean serverConnected;
	private JButton loginButton, registerButton, findButton;
	private JLabel noServer;

	public UnregStartPanel() {
		initializeVariables();
		createGUI();
		addActions();
	}
	
	private void initializeVariables() {
		serverConnected = DTCClient.getClient().isServerConnected();
		loginButton = new JButton("Log In");
		loginButton.setFont(new Font("Calibri", Font.PLAIN, 14));
		loginButton.setBackground(new Color(0xfebf06));
		loginButton.setForeground(Color.white);
        // customize the button with your own look
		loginButton.setUI(new RegStyledButtonUI());
		registerButton = new JButton("Register");
		registerButton.setFont(new Font("Calibri", Font.PLAIN, 14));
		registerButton.setBackground(new Color(0xfebf06));
		registerButton.setForeground(Color.white);
        // customize the button with your own look
		registerButton.setUI(new RegStyledButtonUI());
		findButton = new JButton("Find Friends");
		findButton.setFont(new Font("Calibri", Font.PLAIN, 14));
		findButton.setBackground(new Color(0xfebf06));
		findButton.setForeground(Color.white);
        // customize the button with your own look
		findButton.setUI(new RegStyledButtonUI());
		noServer = new JLabel("Server is not connected");
	}
	
	private void createGUI() {
		add(noServer, BorderLayout.NORTH);
		add(loginButton, BorderLayout.CENTER);
		add(registerButton, BorderLayout.CENTER);
		add(findButton, BorderLayout.SOUTH);
		noServer.setVisible(false);
		if (!serverConnected) {
			loginButton.setEnabled(false);
			registerButton.setEnabled(false);
			findButton.setEnabled(false);
			noServer.setVisible(true);
		}
	}
	
	private void addActions() {
		loginButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DTCClient.getUnregGUI().changeCard("LOGIN");
			}
		});
		
		registerButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DTCClient.getUnregGUI().changeCard("REGISTER");
			}
		});
		
		findButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DTCClient.getUnregGUI().changeCard("FIND");
			}
		});
		
	}
	
	public void paintComponent(Graphics g) {
		try {
			BufferedImage background = ImageIO.read(new File("resources/img/background.png/"));
			g.drawImage(background, 0, 0, getWidth(), getHeight(), null);
		} catch (IOException e) {}
	}

}
