package client;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class UnregLoginPanel extends JPanel {
	
	private static final long serialVersionUID = 2475183187257419393L;

	private JLabel usernameLabel, passwordLabel;
	private JButton loginButton, backButton;
	private JTextField usernameField;
	private JPasswordField passwordField;
	private BoxLayout boxLayout;

	public UnregLoginPanel() {
		initializeVariables();
		createGUI();
		addActions();
	}
	
	public void paintComponent(Graphics g) {
		try {
			BufferedImage background = ImageIO.read(new File("resources/img/background2.png/"));
			g.drawImage(background, 0, 0, getWidth(), getHeight(), null);
		} catch (IOException e) {}
	}
	
	private void initializeVariables() {
		usernameLabel = new JLabel("Username");
		usernameLabel.setForeground(Color.WHITE);
		passwordLabel = new JLabel("Password");
		passwordLabel.setForeground(Color.WHITE);
		loginButton = new JButton("Log In");
		loginButton.setFont(new Font("Calibri", Font.PLAIN, 14));
		loginButton.setBackground(new Color(0xfebf06));
		loginButton.setForeground(Color.white);
        // customize the button with your own look
		loginButton.setUI(new RegStyledButtonUI());
		usernameField = new JTextField();
		passwordField = new JPasswordField();
		passwordField.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
				if(e.getKeyChar() == '\n') {
					login();
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {}

			@Override
			public void keyReleased(KeyEvent e) {}
			
		});
		backButton = new JButton("Back");
		backButton.setFont(new Font("Calibri", Font.PLAIN, 14));
		backButton.setBackground(new Color(0xfebf06));
		backButton.setForeground(Color.white);
        // customize the button with your own look
		backButton.setUI(new RegStyledButtonUI());
		boxLayout = new BoxLayout(this, BoxLayout.PAGE_AXIS);
	}
	
	private void createGUI() {
		setLayout(boxLayout);
		add(Box.createRigidArea(new Dimension(200,200)));
		usernameField.setMaximumSize(new Dimension(250,30));
		passwordField.setMaximumSize(new Dimension(250,30));
		add(usernameLabel);
		add(usernameField);
		add(passwordLabel);
		add(passwordField);
		add(loginButton);
		add(Box.createVerticalStrut(40));
		add(backButton);
	}
	
	private void addActions() {
		backButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DTCClient.getUnregGUI().changeCard("START");
			}
		});
		
		loginButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				login();
			}
		});
		
	}
	
	private void login() {
		DTCClient.getClient().login(usernameField.getText(), new String(passwordField.getPassword()));
	}

}
