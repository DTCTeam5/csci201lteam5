package client;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class UnregFindPanel extends JPanel {

	private static final long serialVersionUID = 4896649225909804931L;
	private JLabel usernameLabel, emailLabel, phoneLabel;
	private JTextField usernameField, emailField, phoneField;
	private JButton usernameButton, emailButton, phoneButton, backButton;
	private BoxLayout boxLayout;

	public UnregFindPanel() {
		initializeVariables();
		createGUI();
		addActions();
	}
	
	public void paintComponent(Graphics g) {
		try {
			BufferedImage background = ImageIO.read(new File("resources/img/background2.png/"));
			g.drawImage(background, 0, 0, getWidth(), getHeight(), null);
		} catch (IOException e) {}
	}

	private void initializeVariables() {
		usernameLabel = new JLabel("Username");
		usernameLabel.setForeground(Color.WHITE);
		usernameField = new JTextField();
		usernameButton = new JButton("Search By Username");
		usernameButton.setFont(new Font("Calibri", Font.PLAIN, 14));
		usernameButton.setBackground(new Color(0xfebf06));
		usernameButton.setForeground(Color.white);
        // customize the button with your own look
		usernameButton.setUI(new RegStyledButtonUI());
		emailLabel = new JLabel("Email");
		emailLabel.setForeground(Color.WHITE);
		emailField = new JTextField();
		emailButton = new JButton("Search By Email");
		emailButton.setFont(new Font("Calibri", Font.PLAIN, 14));
		emailButton.setBackground(new Color(0xfebf06));
		emailButton.setForeground(Color.white);
        // customize the button with your own look
		emailButton.setUI(new RegStyledButtonUI());
		phoneLabel = new JLabel("Phone");
		phoneLabel.setForeground(Color.WHITE);
		phoneField = new JTextField();
		phoneButton = new JButton("Search By Phone");
		phoneButton.setFont(new Font("Calibri", Font.PLAIN, 14));
		phoneButton.setBackground(new Color(0xfebf06));
		phoneButton.setForeground(Color.white);
        // customize the button with your own look
		phoneButton.setUI(new RegStyledButtonUI());
		backButton = new JButton("Back");
		backButton.setFont(new Font("Calibri", Font.PLAIN, 14));
		backButton.setBackground(new Color(0xfebf06));
		backButton.setForeground(Color.white);
        // customize the button with your own look
		backButton.setUI(new RegStyledButtonUI());
		boxLayout = new BoxLayout(this, BoxLayout.PAGE_AXIS);
	}
	
	private void createGUI() {
		setLayout(boxLayout);
		add(Box.createRigidArea(new Dimension(300,100)));
		usernameField.setMaximumSize(new Dimension(250,30));
		add(usernameLabel);
		add(usernameField);
		add(usernameButton);
		emailField.setMaximumSize(new Dimension(250,30));
		add(emailLabel);
		add(emailField);
		add(emailButton);
		phoneField.setMaximumSize(new Dimension(250,30));
		add(phoneLabel);
		add(phoneField);
		add(phoneButton);
		add(Box.createVerticalStrut(40));
		add(backButton);
	}
	
	private void addActions() {
		backButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DTCClient.getUnregGUI().changeCard("START");
			}
		});
		
		usernameButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				usernameSearch();
			}
		});
		
		emailButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				emailSearch();
			}
		});
		
		phoneButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				phoneSearch();
			}
		});
	}
	private void usernameSearch(){
		DTCClient.getClient().usernameSearch(usernameField.getText());
	}
	
	private void emailSearch(){
		DTCClient.getClient().emailSearch(emailField.getText());
	}
	private void phoneSearch(){
		DTCClient.getClient().phoneSearch(phoneField.getText());
	}
	
}
