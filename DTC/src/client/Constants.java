package client;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class Constants {
	public static final String resourceFilepath = new String("resources/");
	public static final String ClientConfigFilepath = new String("ClientConfig.properties/");
	public static final String iconFilepath = new String("img/icon/office.png/");
	public static final Integer unregGUIWidth = 700;
	public static final Integer unregGUIHeight = 500;
	public static final String imageFiles = "users/";
	
	// prints everytime client sends something
	public static final boolean DEBUGMODE = true;
	
	public static String encrypt(String unencryptedPassword) {
		MessageDigest md = null;
	    try {
	      md = MessageDigest.getInstance("SHA");
	    } catch(NoSuchAlgorithmException e) {
	      System.out.println(e.getMessage());
	    } try {
	      md.update(unencryptedPassword.getBytes("UTF-8"));
	    } catch(UnsupportedEncodingException e) {
	    	System.out.println(e.getMessage());
	    }
	    byte raw[] = md.digest();
	    String hash = (Base64.getEncoder()).encodeToString(raw);
	    return hash;
	  }
}
	