package client;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import message.UpcommingPlan;

public class RegPlanRequests extends JPanel{
	private static final long serialVersionUID = 1L;
	private UpcommingPlan plan;
	private JButton planName;
	private boolean accepted;
	
	RegPlanRequests(UpcommingPlan plan) {
		this.plan = plan;
		accepted = false;
		setLayout(new GridLayout(3,3));
		createGUI();
		addActions();
	}
	
	private void createGUI(){
		
		JPanel overAllPanel = new JPanel();
		overAllPanel.setLayout(new GridLayout(3,3));
		
		planName = new JButton(plan.getTitle());
		planName.setFont(new Font("Calibri", Font.PLAIN, 14));
		planName.setBackground(new Color(0xe6e600));
		planName.setForeground(Color.white);
		planName.setUI(new RegStyledButtonUI());
		//planName.setText(plan.getTitle());
		
		overAllPanel.add(planName);
		overAllPanel.add(new JPanel());
		overAllPanel.add(new JPanel());
		
		JLabel timeLabel = new JLabel("Time: " + plan.getTime());
		JLabel locationLabel = new JLabel("Location: " + plan.getStreetAddress());
		JPanel timeAndLocationPanel = new JPanel();
		GridLayout panelLayout = new GridLayout(2,1);
		timeAndLocationPanel.setLayout(panelLayout);
		timeAndLocationPanel.add(timeLabel);
		timeAndLocationPanel.add(locationLabel);
		
		
		overAllPanel.add(timeAndLocationPanel);
		overAllPanel.add(new JPanel());
		
		add(overAllPanel);
		
	}
	public void setAcceptedPlan(boolean b){
		accepted = b;
	}
	public boolean getResultPlan(){
		return accepted;
	}
	
	public void addActions(){
		planName.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				new RegPlanRequestClicked(plan.getTitle(), plan.getStreetAddress(), 
						plan.getTime().toString(),RegPlanRequests.this);
			}
		});
	}
	
	public int getPlanID() {
		return plan.getPlanID();
	}
	
	public UpcommingPlan getPlan() {
		return plan;
	}
	public void setPlan(UpcommingPlan plan) {
		this.plan = plan;
	}
}
