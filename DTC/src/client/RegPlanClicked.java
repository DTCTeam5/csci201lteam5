package client;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import message.ChatMessage;
import message.DTCUser;
import message.RejectUpcomingPlan;
import message.UpcommingPlan;
import message.UpcommingPlanResponse;

public class RegPlanClicked extends JDialog{
	
	
	private static final long serialVersionUID = 1L;
	private JPanel topPanel;
	private JPanel centerPanel;
	private JPanel eastWithinCenter;
	private JPanel westWithinCenter;
	private JLabel planName;
	private JLabel dateAndTime;
	private JLabel location;
	private JButton leavePlan;
	private JTextArea chatArea;
	private JTextField chatField;
	private JButton enter;
	private JScrollPane jsp;
	private String eventName;
	private String eventTime;
	private String eventLocation;
	private RegUpcommingPlanPanel rupp;
	private RegPlanClicked accessible;
	private Vector<String> goingFriends;
	private Vector<String> invitedFriends;
	private UpcommingPlan plan;
	
	public RegPlanClicked(UpcommingPlan plan, RegUpcommingPlanPanel rupp){
		this.eventName = plan.getTitle();
		this.eventTime = plan.getTime().toString();
		this.eventLocation = plan.getStreetAddress();
		this.rupp = rupp;
		this.plan = plan;
		goingFriends = plan.getGoingFriends();
		invitedFriends = plan.getInviteduserNames();
		accessible = this;
		initializeComponents();
		createGUI();
		addActions();
		
		DTCClient.getClient().getRegGUI().addTextArea(plan.getPlanID(), chatArea);
	}
	
	public void initializeComponents(){
		topPanel = new JPanel();
		topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.X_AXIS));
		centerPanel = new JPanel();
		centerPanel.setLayout(new BorderLayout());
		eastWithinCenter = new JPanel();
		//eastWithinCenter.setLayout(new BoxLayout(eastWithinCenter, BoxLayout.Y_AXIS));
		eastWithinCenter.setLayout(new GridLayout(8, 1));
		westWithinCenter = new JPanel();
		westWithinCenter.setLayout(new BorderLayout());
		planName = new JLabel(eventName);
		dateAndTime = new JLabel(eventTime);
		location = new JLabel(eventLocation);
		leavePlan = new JButton("Leave Plan");
		chatArea = new JTextArea();
		chatArea.setText(DTCClient.getClient().getRegGUI().getCurrData(plan.getPlanID()));
		chatArea.setEditable(false);
		chatField = new JTextField(10);
		chatField.addKeyListener(new KeyListener() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if(arg0.getKeyChar() == '\n') {
					sendMessage();
				}
			}
			@Override
			public void keyReleased(KeyEvent arg0) {}
			@Override
			public void keyTyped(KeyEvent arg0) {}
		});
		enter = new JButton("Enter");
		enter.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				sendMessage();
			}
		});
		jsp = new JScrollPane(chatArea);
		jsp.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				DTCClient.getClient().getRegGUI().removeTextArea(plan.getPlanID());
				RegPlanClicked.this.dispose();
		    }
		});
	}
	
	private void sendMessage() {
		ChatMessage message = new ChatMessage(DTCClient.getClient().getMe().getUserName(), plan.getPlanID(), chatField.getText() + '\n');
		DTCClient.getClient().sendMessage(message);
		chatField.setText("");
	}
	
	public void addActions(){
		leavePlan.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				DTCUser user = DTCClient.getClient().getMe();
				RejectUpcomingPlan rup = new RejectUpcomingPlan(rupp.getPlanID(), user.getUserName());
				DTCClient.getClient().sendMessage(rup);
				accessible.dispose();
			}
		});
	}
	
	
	
	public void createGUI(){
		setSize(800, 800);
		setLocation(100, 100);
		setLayout(new BorderLayout());
		topPanel.add(planName);
		topPanel.add(Box.createHorizontalStrut(5));
		topPanel.add(new JLabel("|"));
		topPanel.add(Box.createHorizontalStrut(5));
		topPanel.add(dateAndTime);
		topPanel.add(Box.createHorizontalStrut(5));
		topPanel.add(new JLabel("|"));
		topPanel.add(Box.createHorizontalStrut(5));
		topPanel.add(location);

		JPanel jp1 = new JPanel();
		jp1.setLayout(new BoxLayout(jp1, BoxLayout.X_AXIS));
		jp1.add(new JLabel("Created By: " + plan.getCreatorUsername()));
		jp1.add(Box.createHorizontalStrut(5));
		eastWithinCenter.add(jp1);
		eastWithinCenter.add(new JLabel("Going"));
		JPanel jp2 = new JPanel();
		//jp2.setLayout(new GridLayout(3, 4));
		
		// add Images here for friends that are going
		for(String friendName : goingFriends) {
			jp2.add(new JLabel(friendName));
		}
		eastWithinCenter.add(jp2);
		
		eastWithinCenter.add(jp2);
		eastWithinCenter.add(new JLabel("Invited"));
		JPanel jp3 = new JPanel();
		//jp3.setLayout(new GridLayout(3, 4));
		for(String friendName : invitedFriends) {
			jp3.add(new JLabel(friendName));
		}
		eastWithinCenter.add(jp3);
		
		// add Images here for friends that are invited
		
		//eastWithinCenter.add(new JLabel("Rejected"));
		// nah
		JPanel jp4 = new JPanel();
		jp4.setLayout(new GridLayout(3, 4));
		
		// add Images here for friends that rejected the invitation
		
		JPanel planButtons = new JPanel();
		planButtons.setLayout(new BoxLayout(planButtons, BoxLayout.X_AXIS));
		planButtons.add(leavePlan);
		eastWithinCenter.add(planButtons);
		
		westWithinCenter.add(new JLabel("Chat"), BorderLayout.NORTH);
		westWithinCenter.add(jsp, BorderLayout.CENTER);
		JPanel chatPanel = new JPanel();
		chatPanel.setLayout(new BoxLayout(chatPanel, BoxLayout.X_AXIS));
		chatPanel.add(chatField);
		chatPanel.add(enter);
		westWithinCenter.add(chatPanel, BorderLayout.SOUTH);
		
		
		centerPanel.add(eastWithinCenter, BorderLayout.WEST);
		centerPanel.add(westWithinCenter, BorderLayout.EAST);
		
		add(topPanel, BorderLayout.NORTH);
		add(centerPanel, BorderLayout.CENTER);
		
		
		
		setVisible(true);
		setSize(new Dimension(300,300));
	}
}

