package client;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import message.ChatMessage;
import message.DTCUser;
import message.FriendRequest;
import message.RejectUpcomingPlan;
import message.UpcommingPlan;
import message.UpcommingPlanResponse;


public class RegClientGUI extends JFrame {
	
	
	
	private static final long serialVersionUID = 1L;
	private JPanel topPanel;
	private JPanel middlePanel;
	private JPanel middleCenter;
	private Box acceptedPlanBox;
	private Box middlePanelBox;
	private JPanel bottomPanel;
	private Box unacceptedPlanBox;
	private JLabel planRequestLabel;
	private JLabel upcommingPlansLabel;
	private JPanel toolBarPanel;
	private JScrollPane topScrollPane;
	private JPanel friendsScrollPanel;
	private JScrollPane upcomingPane;
	JButton createPlanButton;
	private JScrollPane scrollPanel;
	private JScrollPane requestPane;
	private RegFriendRequestPanel friendRequestPanel;
	private HashMap<String, RegFriendPanel> friendMap;
	private HashMap<Integer, RegUpcommingPlanPanel> upcomingPlanMap;
	private HashMap<Integer, RegPlanRequests> requestedPlanMap;
	private HashMap<Integer, JTextArea> chatMap;
	private HashMap<Integer, StringBuffer> chatDataMap;
	private int friendRequestCount;
	private RegTrynaPanel regTrynaPanel;
	
	
	public RegClientGUI(){
		initializeComponents();
		createGUI();
		addActions();
	}
	
	
	public void initializeComponents(){

		topPanel = new JPanel();
		middlePanel = new JPanel();
		bottomPanel = new JPanel();
		planRequestLabel = new JLabel("Plan Requests");
		upcommingPlansLabel = new JLabel("Upcomming Plans");
		toolBarPanel = new JPanel();
		topScrollPane = new JScrollPane();
		friendsScrollPanel = new JPanel();
		upcomingPane = new JScrollPane();
		requestPane = new JScrollPane();
		friendRequestPanel = new RegFriendRequestPanel();
		friendMap = new HashMap<String, RegFriendPanel>();
		upcomingPlanMap = new HashMap<Integer, RegUpcommingPlanPanel>();
		requestedPlanMap = new HashMap<Integer, RegPlanRequests>();
		createPlanButton = new JButton("Create Plan");
		friendRequestCount = 0;
		
		chatMap = new HashMap<Integer, JTextArea>();
		chatDataMap = new HashMap<Integer, StringBuffer>();
	}
	
	public void createGUI(){
		
		setSize(1000, 850);
		setLocation(200, 0);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		GridLayout overallLayout = new GridLayout(3,1);
		this.setLayout(overallLayout);
		createTopPanel();
		createMiddlePanel();
		createBottomPanel();
		setVisible(true);
	}
	
	private void createTopPanel(){
		
		BorderLayout topLayout = new BorderLayout();
		topPanel.setLayout(topLayout);
		regTrynaPanel =  new RegTrynaPanel();
		toolBarPanel.setLayout(new BoxLayout(toolBarPanel,BoxLayout.X_AXIS));
		toolBarPanel.add(regTrynaPanel);
		toolBarPanel.add(new RegAddFriendPanel());
		toolBarPanel.add(Box.createHorizontalStrut(6));
		toolBarPanel.add(friendRequestPanel);
		
		topPanel.add(toolBarPanel, BorderLayout.NORTH);
	//	topScrollPane.setBackground(Color.pink);
		topScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

		topScrollPane.getViewport().add(friendsScrollPanel);
		
		updateFriendsListPanel();
		
		topPanel.add(topScrollPane, BorderLayout.CENTER);
		this.add(topPanel);
		//add Jscrollpane to borderlayout center
	}
	
	public void addToFriendsList(DTCUser friend) {
		DTCUser me = DTCClient.getClient().getMe();
		me.addFriend(friend);
		updateFriendsListPanel();
	}
	
	public void updateFriendStatus(String username,int status){
		RegFriendPanel friendPanel = friendMap.get(username);
		if(friendPanel != null) {
			friendPanel.setOnlinePanel(status);
		}
		
	}
	
	public void updateFriendTryna(String username, String tryna) {
		RegFriendPanel friendPanel = friendMap.get(username);
		friendPanel.setTryna(tryna);
		update();
	}
	
	private void updateFriendsListPanel() {
		friendsScrollPanel.removeAll();
		friendMap.clear();
		DTCUser me = DTCClient.getClient().getMe();
		for(DTCUser friend : me.getFriendsList()) {
			RegFriendPanel friendPanel = new RegFriendPanel(friend,regTrynaPanel);
			friendMap.put(friend.getUserName(), friendPanel);
			friendsScrollPanel.add(friendPanel);
		}
		friendsScrollPanel.repaint();
		friendsScrollPanel.revalidate();
	}
	
	// accepted Plans
	private void createMiddlePanel(){
		middlePanel.setLayout(new BorderLayout());
		middlePanel.add(upcommingPlansLabel, BorderLayout.NORTH);
		

		middlePanelBox = Box.createHorizontalBox();
//		RegUpcommingPlanPanel rup = new RegUpcommingPlanPanel(null);
//		horizontalBox1.add(rup);
		upcomingPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

		upcomingPane.getViewport().add(middlePanelBox);

		middlePanelBox.add(new JLabel("yoofi"));
//		scrollPanel.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
//		scrollPanel.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
//		middlePanel.add(scrollPanel, BorderLayout.CENTER);
		
		middlePanel.add(createPlanButton, BorderLayout.EAST);
		updateUpcommingPlans();
		
		
		JPanel eastPanel = new JPanel();
		JButton createPlanButton = new JButton("Create Plan");
		createPlanButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new RegNewPlan();
			}
		});
		eastPanel.add(createPlanButton);
		middlePanel.add(eastPanel, BorderLayout.EAST);
		middlePanel.add(upcomingPane, BorderLayout.CENTER);

		
		this.add(middlePanel);
	}
	
	// pending Plans
	private void createBottomPanel(){
		bottomPanel.setLayout(new BorderLayout());
		bottomPanel.add(planRequestLabel, BorderLayout.NORTH);
		
		unacceptedPlanBox = Box.createHorizontalBox();
		requestPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);

		requestPane.getViewport().add(unacceptedPlanBox);
		updatePendingPlans();
		
		bottomPanel.add(requestPane, BorderLayout.CENTER);
		
		this.add(bottomPanel);
	}
	
	private void addActions() {
		createPlanButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new RegNewPlan();
			}
		});
	}
	
	public void addFriendRequest(FriendRequest request) {
		friendRequestPanel.addFriendRequest(request);
	}
	
	public void addAcceptedPlan(UpcommingPlan plan) {
		RegUpcommingPlanPanel upcommingPlanPanel = upcomingPlanMap.get(plan.getPlanID());
		if(upcommingPlanPanel == null) {
			upcomingPlanMap.put(plan.getPlanID(), new RegUpcommingPlanPanel(plan));
		}
		else {
			RegUpcommingPlanPanel planPanel = upcomingPlanMap.get(plan.getPlanID());
			planPanel.setPlan(plan);
		}
		updateUpcommingPlans();
	}
	
	public void removeFriendFromAcceptedPlan(RejectUpcomingPlan rejectUpcomingPlan) {
		RegUpcommingPlanPanel upcommingPlanPanel = upcomingPlanMap.get(rejectUpcomingPlan.getPlanID());
		if(upcommingPlanPanel != null) {
			upcommingPlanPanel.getPlan().removeFriend(rejectUpcomingPlan.getUsername());
		}
	}
	
	private void updateUpcommingPlans() {
		middlePanelBox.removeAll();
		for(RegUpcommingPlanPanel planPanel : upcomingPlanMap.values()) {
			middlePanelBox.add(planPanel);
		}
		update();
	}

	public void addNotAcceptedPlan(UpcommingPlan plan) {
		RegPlanRequests requestedPlanel = requestedPlanMap.get(plan.getPlanID());
		if(requestedPlanel == null) {
			requestedPlanMap.put(plan.getPlanID(), new RegPlanRequests(plan));
		}
		updatePendingPlans();
	}
	public void updateDTCColor(int color,String username){
		topPanel.remove(regTrynaPanel);
	}
	private void updatePendingPlans() {
		unacceptedPlanBox.removeAll();
		for(RegPlanRequests requestedPanel : requestedPlanMap.values()){
			unacceptedPlanBox.add(requestedPanel);
		}
		update();
	}
	
	public void dealWithPlanResponse(UpcommingPlanResponse upcommingPlanResponse) {
		int planID = upcommingPlanResponse.getPlanID();
		if(upcommingPlanResponse.getResponse()) {
			if(upcommingPlanResponse.getPlan() == null) {
				upcomingPlanMap.put(planID, new RegUpcommingPlanPanel(requestedPlanMap.get(planID)));
			}
			else {
				upcomingPlanMap.put(planID, new RegUpcommingPlanPanel(upcommingPlanResponse.getPlan()));
			}
		}
		requestedPlanMap.remove(planID);
		updateUpcommingPlans();
		updatePendingPlans();
	}
	
	public void dealWithRejectingUpcomingPlan(RejectUpcomingPlan rup){
		int planID = rup.getPlanID();
		upcomingPlanMap.remove(planID);
		
		updateUpcommingPlans();
		updatePendingPlans();
	}
	private void update() {
		repaint();
		revalidate();
	}
	
	public int getFriendRequestCount() {
		return friendRequestCount;
	}
	
	public void incrimentFriendRequestCount() {
		++friendRequestCount;
	}
	
	public void decimentFriendRequestCount() {
		--friendRequestCount;
	}
	
	public void addTextArea(int planID, JTextArea chatArea) {
		chatMap.put(planID, chatArea);
	}
	
	public void removeTextArea(int planID) {
		chatMap.remove(planID);
	}
	
	public void updateChat(ChatMessage chatMessage) {
		saveChatToMap(chatMessage);
		JTextArea chatBox = chatMap.get(chatMessage.getPlanID());
		if(chatBox == null) {
			return;
		}
		
		chatBox.append(chatMessage.getMessage());
	}
	
	private void saveChatToMap(ChatMessage chatMessage) {
		int planID = chatMessage.getPlanID();
		String message = chatMessage.getMessage();
		StringBuffer currMessage = chatDataMap.get(planID);
		if(currMessage == null) chatDataMap.put(planID, new StringBuffer(message));
		else {
			currMessage.append(message);
		}
	}
	
	public String getCurrData(int planID) {
		if(chatDataMap.get(planID) == null) return null;
		return chatDataMap.get(planID).toString();
	}
}
