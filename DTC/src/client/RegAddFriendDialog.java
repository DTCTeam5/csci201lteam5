package client;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import constants.SpringUtilities;
import message.DTCUser;
import message.EmailSearch;
import message.FoundUser;
import message.FriendRequest;
import message.PhoneSearch;
import message.UsernameSearch;

public class RegAddFriendDialog {
	private static RegAddFriendDialog friendSearch;
	private JDialog mainDialog;
	private JPanel resultPanel;
	
	static {
		friendSearch = new RegAddFriendDialog();
	}
	
	public static RegAddFriendDialog getDialog() {
		return friendSearch;
	}
	
	private RegAddFriendDialog() { 
		initVariable();
		createGUI();
		mainDialog.setVisible(false);
	}
	
	private void initVariable() {
		mainDialog = new JDialog();
		resultPanel = new JPanel();
	}
	
	private void createGUI() {
		mainDialog.setTitle("Add a friend...");
		
		JPanel formPanel = new JPanel(new SpringLayout());
		JTextField usernameTextField = new JTextField(30);
		usernameTextField.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
				resultPanel.removeAll();
				mainDialog.repaint();
				mainDialog.revalidate();
				mainDialog.pack();
				if(e.getKeyChar() == '\n' && !usernameTextField.getText().isEmpty() && !usernameTextField.getText().equals(DTCClient.getClient().getMe().getUserName())) {
					UsernameSearch search = new UsernameSearch(usernameTextField.getText());
					DTCClient.getClient().sendMessage(search);
				}
			}
			@Override
			public void keyPressed(KeyEvent e) {}
			@Override
			public void keyReleased(KeyEvent e) {}
		});
		JLabel usernameLabel = new JLabel("Username:");
		usernameLabel.setLabelFor(usernameTextField);
		JTextField emailTextField = new JTextField(30);
		emailTextField.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
				resultPanel.removeAll();
				mainDialog.repaint();
				mainDialog.revalidate();
				mainDialog.pack();
				if(e.getKeyChar() == '\n' && !emailTextField.getText().isEmpty()) {
					EmailSearch search = new EmailSearch(emailTextField.getText());
					DTCClient.getClient().sendMessage(search);
				}
			}
			@Override
			public void keyPressed(KeyEvent e) {}
			@Override
			public void keyReleased(KeyEvent e) {}
		});
		JLabel emailLabel = new JLabel("Email:");
		emailLabel.setLabelFor(emailTextField);
		JTextField phoneTextField = new JTextField(30);
		phoneTextField.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
				resultPanel.removeAll();
				mainDialog.repaint();
				mainDialog.revalidate();
				mainDialog.pack();
				if(e.getKeyChar() == '\n' && !phoneTextField.getText().isEmpty()) {
					PhoneSearch search = new PhoneSearch(phoneTextField.getText());
					DTCClient.getClient().sendMessage(search);
				}
			}
			@Override
			public void keyPressed(KeyEvent e) {}
			@Override
			public void keyReleased(KeyEvent e) {}
		});
		JLabel phoneLabel = new JLabel("Phone Number:");
		phoneLabel.setLabelFor(phoneTextField);
		
		formPanel.add(usernameLabel);
		formPanel.add(usernameTextField);
		formPanel.add(emailLabel);
		formPanel.add(emailTextField);
		formPanel.add(phoneLabel);
		formPanel.add(phoneTextField);
		
		SpringUtilities.makeCompactGrid(formPanel, 3, 2, 6, 6, 6, 6);
		
		mainDialog.add(formPanel, BorderLayout.CENTER);
		mainDialog.add(resultPanel, BorderLayout.SOUTH);
		mainDialog.pack();
	}
	
	public void showFriendsList() {
		mainDialog.setVisible(true);
	}
	
	public void addSearchResult(FoundUser user) {
		resultPanel.removeAll();
		resultPanel.add(new JLabel(user.getUsername()));
		JButton addButton = new JButton("Add");
		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				FriendRequest friendRequest = new FriendRequest(DTCClient.getClient().getMe().getUserName(), user.getUsername());
				DTCClient.getClient().sendMessage(friendRequest);
				mainDialog.setVisible(false);
			}
		});
		resultPanel.add(addButton);
		mainDialog.repaint();
		mainDialog.revalidate();
		mainDialog.pack();
	}
}
