package client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.Vector;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import message.FriendRequest;

public class RegFriendRequestPanel extends JPanel{
	
	private static final long serialVersionUID = 1L;
	private JButton friendRequestButton;
	private RegFriendRequestDialog dialog;
	private HashMap<String, FriendRequest> friendRequests;
	
	public RegFriendRequestPanel(){
		initializeComponents();
		createGUI();
		addActions();
	}
	
	public void initializeComponents(){
		friendRequests = new HashMap<String, FriendRequest>();
		friendRequestButton = new JButton("0 Friend Requests");
		dialog = new RegFriendRequestDialog(this);
	}
	
	public void createGUI(){
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		add(friendRequestButton);
	}
	
	private void addActions() {
		friendRequestButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dialog.displayRequests(friendRequests);
			}
		});
	}
	
	private void update() {
		repaint();
		revalidate();
	}

	public void addFriendRequest(FriendRequest friendRequest) {
		DTCClient.getRegGUI().incrimentFriendRequestCount();
		friendRequests.put(friendRequest.getFromID(), friendRequest);
		friendRequestButton.setText(DTCClient.getRegGUI().getFriendRequestCount() + " Friend Requests");
		update();
	}
	
	public void removeFriendRequest(FriendRequest friendRequest) {
		DTCClient.getRegGUI().decimentFriendRequestCount();
		friendRequests.remove(friendRequest.getFromID());
		friendRequestButton.setText(DTCClient.getRegGUI().getFriendRequestCount() + " Friend Requests");
		update();
	}
}
