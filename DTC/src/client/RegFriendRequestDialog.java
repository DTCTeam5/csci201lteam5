package client;

import java.awt.Dialog;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import message.FriendRequest;
import message.FriendRequestResponse;

public class RegFriendRequestDialog extends JDialog {
	private static final long serialVersionUID = -3713070297138330097L;
	private HashMap<String, JPanel> requests;
	private JPanel mainPanel;
	private RegFriendRequestPanel parent;
	public RegFriendRequestDialog(RegFriendRequestPanel parent) {
		this.parent = parent;
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		setVisible(false);
		setSize(250, 400);
		mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		add(mainPanel);
		requests = new HashMap<String, JPanel>();
	}
	
	public void displayRequests(HashMap<String, FriendRequest> friendRequests) {
		mainPanel.removeAll();
		
		for(FriendRequest friend : friendRequests.values()) {
			JPanel friendPanel = createsingleRequestField(friend);
			requests.put(friend.getFromID(), friendPanel);
			mainPanel.add(friendPanel);
		}
		update();
		
		setVisible(true);
	}

	private JPanel createsingleRequestField(FriendRequest friendRequest) {
		JPanel main = new JPanel();
		main.setLayout(new BoxLayout(main, BoxLayout.X_AXIS));
		main.add(new JLabel(friendRequest.getFromID()));
		JButton addButton = new JButton("Add");
		addButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				FriendRequestResponse response = new FriendRequestResponse(friendRequest, true);
				DTCClient.getClient().sendMessage(response);
				mainPanel.remove(requests.get(friendRequest.getFromID()));
				requests.remove(friendRequest.getFromID());	
				parent.removeFriendRequest(friendRequest);
				update();
			}
		});
		JButton removeButton = new JButton("Remove");
		removeButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				FriendRequestResponse response = new FriendRequestResponse(friendRequest, false);
				DTCClient.getClient().sendMessage(response);
				mainPanel.remove(requests.get(friendRequest.getFromID()));
				requests.remove(friendRequest.getFromID());	
				parent.removeFriendRequest(friendRequest);
				update();
			}
		});
		main.add(addButton);
		main.add(removeButton);
		
		return main;
	}
	
	private void update() {
		repaint();
		revalidate();
		pack();
		
		if(mainPanel.getComponentCount() == 0) {
			RegFriendRequestDialog.this.dispose();
		}
	}
}
