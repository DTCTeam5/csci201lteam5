package client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Timestamp;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import message.UpcommingPlan;

public class RegUpcommingPlanPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private RegPlanClicked planClicked;
	private UpcommingPlan plan;
	private JButton planName;
	
	RegUpcommingPlanPanel(UpcommingPlan plan) {
		this.plan = plan;
		
		setLayout(new GridLayout(3,3));
		createGUI();
		addActions();
	}
	
	RegUpcommingPlanPanel(RegPlanRequests planRequest) {
		this.plan = planRequest.getPlan();
		setLayout(new GridLayout(3,3));
		createGUI();
		addActions();
	}
	
	
	
	private void createGUI(){
		
		JPanel overAllPanel = new JPanel();
		overAllPanel.setLayout(new GridLayout(3,3));
		setMinimumSize(new Dimension(150, 150));
		
		planName = new JButton(plan.getTitle());
		planName.setFont(new Font("Calibri", Font.PLAIN, 14));
		planName.setBackground(new Color(0x2dce98));
		planName.setForeground(Color.white);
        // customize the button with your own look
		planName.setUI(new RegStyledButtonUI());
		overAllPanel.add(planName);
		overAllPanel.add(new JPanel());
		overAllPanel.add(new JPanel());
		
		JLabel timeLabel = new JLabel("Time: " + plan.getTime());
		JLabel locationLabel = new JLabel("Location: " + plan.getStreetAddress());
		JPanel timeAndLocationPanel = new JPanel();
		GridLayout panelLayout = new GridLayout(2,1);
		timeAndLocationPanel.setLayout(panelLayout);
		timeAndLocationPanel.add(timeLabel);
		timeAndLocationPanel.add(locationLabel);
		
		
		overAllPanel.add(timeAndLocationPanel);
		overAllPanel.add(new JPanel());
		
		add(overAllPanel);
	}
	
	private void addActions(){
		planName.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				planClicked = new RegPlanClicked(plan, RegUpcommingPlanPanel.this);
			}
		});
	}
	
	public int getPlanID(){
		return plan.getPlanID();
	}
	
	public UpcommingPlan getPlan() {
		return plan;
	}
	
	public void setPlan(UpcommingPlan plan) {
		this.plan = plan;
		updateDialog();
	}
	
	private void updateDialog() {
		if(planClicked != null) {
			planClicked.repaint();
			planClicked.revalidate();			
		}
	}

	/*UpcommingPlan(String title, String description, Timestamp time, String creatorUsername,
			Vector<String> inviteduserNames, String streetAddress, Vector<String> sendToFriends)*/
	/*public static void main(String [] args){
		new RegUpcommingPlanPanel(new UpcommingPlan("Title", "description", null, "Creator user", null, "Stret", null));
	}*/

}
