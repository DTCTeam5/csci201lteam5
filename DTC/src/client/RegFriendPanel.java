package client;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

import message.DTCUser;

// changed this from JFrame
public class RegFriendPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private ImageIcon profilePicture;
	private JPanel DTCPanel;
	private JLabel DTCLabel;
	private JLabel nameLabel;
	private JLabel trynaStatusLabel;
	private JButton deleteFriendButton;
	private JPanel onlineStatusPanel;
	private JPanel outterPanel;
	private JPanel iconAndDTCPanel;
	private JPanel nameAndTrynaPanel;
	private JPanel onlineAndDeletePanel;
	private JLabel iconImageLabel;
	private DTCUser friend;
	private RegOnlineStatusPanel status;
	private RegTrynaPanel rtp;
	
	private String name;
	private String trynaStatus;
	
	public RegFriendPanel(DTCUser friend, RegTrynaPanel rtp){
		this.rtp = rtp;
		this.friend = friend;
		this.name = friend.getfName() + " " + friend.getlName();
		this.trynaStatus = friend.getTrynaStatus();
		profilePicture = friend.getProfilePicture();
		
		initializeComponents();
		createGUI();
	
		
		setVisible(true);
		
	}
	
	public void initializeComponents(){
		
		profilePicture = new ImageIcon("resources/profilePic.gif");
		DTCPanel = new JPanel();
		DTCLabel = new JLabel("DTC", SwingConstants.CENTER);
		nameLabel = new JLabel(name);
		trynaStatusLabel = new JLabel(trynaStatus);
		deleteFriendButton = new JButton("X");
		onlineStatusPanel = new JPanel();
		outterPanel = new JPanel();
		iconAndDTCPanel = new JPanel();
		nameAndTrynaPanel = new JPanel();
		onlineAndDeletePanel = new JPanel();
		

	}
	
	public void createGUI(){
		GridLayout outterGrid = new GridLayout(3,1);
		outterPanel.setLayout(outterGrid);
		Border roundedBorder = new LineBorder(Color.BLACK, 2, true);
		outterPanel.setBorder(roundedBorder);
		
		
		GridLayout iconAndDTCGrid = new GridLayout(1,2);
		DTCLabel.setVerticalAlignment(SwingConstants.CENTER);
		Font font0 = DTCLabel.getFont();
		// same font but bold
		Font boldFont0 = new Font(font0.getFontName(), Font.BOLD, font0.getSize()+4);
		DTCLabel.setFont(boldFont0);
		DTCPanel.setBorder(roundedBorder);
		
		DTCPanel.add(DTCLabel);
		//DTCPanel.setBackground(Color.GREEN);
		iconAndDTCPanel.setLayout(iconAndDTCGrid);
		JLabel pictureLabel = new JLabel(profilePicture);
		pictureLabel.setBorder(roundedBorder);
		iconAndDTCPanel.add(pictureLabel);
		iconAndDTCPanel.add(DTCPanel);
		
		outterPanel.add(iconAndDTCPanel);
		
		GridLayout nameAndTrynaGrid = new GridLayout(2,1);
		nameAndTrynaPanel.setLayout(nameAndTrynaGrid);
		nameAndTrynaPanel.setBorder(BorderFactory.createTitledBorder("DTC Tryna"));
		nameLabel.setHorizontalAlignment(JLabel.CENTER);
		nameAndTrynaPanel.add(nameLabel);
		trynaStatusLabel.setHorizontalAlignment(JLabel.CENTER);
		Font font = trynaStatusLabel.getFont();
		// same font but bold
		Font boldFont = new Font(font.getFontName(), Font.BOLD, font.getSize());
		trynaStatusLabel.setFont(boldFont);
		nameAndTrynaPanel.add(trynaStatusLabel);
		
		outterPanel.add(nameAndTrynaPanel);
		
		onlineAndDeletePanel.setLayout(new GridLayout(2,4));
		onlineStatusPanel.setBackground(Color.RED);
		status = new RegOnlineStatusPanel(friend.getDTCStatus());
		
		onlineAndDeletePanel.add(new JPanel());
		onlineAndDeletePanel.add(new JPanel());
		onlineAndDeletePanel.add(new JPanel());
		onlineAndDeletePanel.add(new JPanel());
		onlineAndDeletePanel.add(status);
		onlineAndDeletePanel.add(new JPanel());
		onlineAndDeletePanel.add(new JPanel());
		onlineAndDeletePanel.add(deleteFriendButton);
		
		outterPanel.add(onlineAndDeletePanel);
		setOnlinePanel(friend.getDTCStatus());
		add(outterPanel);
	}

	public void setOnlinePanel(int newStatus){
		if(newStatus == 2){
			DTCPanel.setBackground(Color.RED);
		}
		else if(newStatus == 0){
			DTCPanel.setBackground(Color.red);
			status.updateStatus(newStatus);
		}
		else if(newStatus == 1){
			DTCPanel.setBackground(Color.GREEN);
			status.updateStatus(newStatus);
		}
	}
	
	public void setTryna(String message) {
		trynaStatusLabel.setText(message);
	}
}

