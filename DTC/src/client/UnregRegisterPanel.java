package client;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

import javax.imageio.ImageIO;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class UnregRegisterPanel extends JPanel {
	
	private static final long serialVersionUID = 79876545610325303L;
	private JLabel usernameLabel, passwordLabel,confirmPasswordLabel, emailLabel, fnameLabel, lnameLabel, phoneLabel;
	private JTextField usernameField, emailField, fnameField, lnameField, phoneField;
	private JPasswordField passwordField, confrimPasswordField;
	private JButton registerButton, backButton, uploadPicture;
	private String username, password, email, fname, lname, phone;
	private ImageIcon profilePicture;
	private BoxLayout boxLayout;
	private JPanel picturePanel;
	private UnregRegisterPanel urp;
	
	public UnregRegisterPanel() {
		initializeVariables();
		createGUI();
		addActions();
	}

	private void initializeVariables() {
		fnameLabel = new JLabel("First Name");
		fnameLabel.setForeground(Color.WHITE);
		lnameLabel = new JLabel("Last Name");
		lnameLabel.setForeground(Color.WHITE);
		emailLabel = new JLabel("Email");
		emailLabel.setForeground(Color.WHITE);
		urp = this;
		phoneLabel = new JLabel("Phone Number");
		phoneLabel.setForeground(Color.WHITE);
		usernameLabel = new JLabel("Username");
		usernameLabel.setForeground(Color.WHITE);
		passwordLabel = new JLabel("Password");
		passwordLabel.setForeground(Color.WHITE);
		confirmPasswordLabel = new JLabel("Confirm Password");
		confirmPasswordLabel.setForeground(Color.WHITE);
		profilePicture = new ImageIcon("resources/img/question.jpg");
		
		fnameField = new JTextField();
		lnameField = new JTextField();
		emailField = new JTextField();
		phoneField = new JTextField();
		usernameField = new JTextField();
		passwordField = new JPasswordField();
		confrimPasswordField = new JPasswordField();
		
		registerButton = new JButton("Register");
		registerButton.setFont(new Font("Calibri", Font.PLAIN, 14));
		registerButton.setBackground(new Color(0xfebf06));
		registerButton.setForeground(Color.white);
        // customize the button with your own look
		registerButton.setUI(new RegStyledButtonUI());
		
		backButton = new JButton("Back");
		backButton.setFont(new Font("Calibri", Font.PLAIN, 14));
		backButton.setBackground(new Color(0xfebf06));
		backButton.setForeground(Color.white);
        // customize the button with your own look
		backButton.setUI(new RegStyledButtonUI());
		
		boxLayout = new BoxLayout(this, BoxLayout.PAGE_AXIS);
	}
	
	private void createGUI() {
		setLayout(boxLayout);
		fnameField.setMaximumSize(new Dimension(250,30));
		lnameField.setMaximumSize(new Dimension(250,30));
		emailField.setMaximumSize(new Dimension(250,30));
		phoneField.setMaximumSize(new Dimension(250,30));
		usernameField.setMaximumSize(new Dimension(250,30));
		passwordField.setMaximumSize(new Dimension(250,30));
		confrimPasswordField.setMaximumSize(new Dimension(250,30));
		
		add(Box.createRigidArea(new Dimension(200,80)));
		add(fnameLabel);
		add(fnameField);
		add(lnameLabel);
		add(lnameField);
		add(emailLabel);
		add(emailField);
		add(phoneLabel);
		add(phoneField);
		add(usernameLabel);
		add(usernameField);
		add(passwordLabel);
		add(passwordField);
		add(confirmPasswordLabel);
		add(confrimPasswordField);
		add(registerButton);
		add(Box.createVerticalStrut(40));
		add(backButton);
	}
	
	private void addActions() {
		backButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DTCClient.getUnregGUI().changeCard("START");
			}
		});
		
		
		registerButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				char[] pass = passwordField.getPassword();
				char[] confirm = confrimPasswordField.getPassword();
				fname = fnameField.getText();
				lname = lnameField.getText();
				email = emailField.getText();
				phone = phoneField.getText();
				username = usernameField.getText();
				password = new String(passwordField.getPassword());
				if(!passFormat(pass))
					JOptionPane.showMessageDialog(DTCClient.getUnregGUI(),
						    "Make sure your password contains an uppercase letter and a number",
						    "Password Error",
						    JOptionPane.WARNING_MESSAGE);
				else if(!passMatch(pass, confirm))
					JOptionPane.showMessageDialog(DTCClient.getUnregGUI(),
						    "Passwords do not match",
						    "Password Error",
						    JOptionPane.WARNING_MESSAGE);
				else{
					DTCClient.getClient().register(fname, lname, email, phone, username, password, profilePicture);
				}
			}
		});
	}
	
	boolean passFormat(char[] entered) {	
		boolean upper = false;
		boolean number = false;
		for (int i = 0; i < entered.length; i++) {
			int ascii = (int)(entered[i]);
			if (ascii > 47 && ascii < 58) {
				number = true;
			}
			if (ascii > 64 && ascii < 91) {
				upper = true;
			}
		}
		if (upper && number)
			return true;
		else
			return false;
	}
	
	boolean passMatch(char[] a, char[] b) {
		if (Arrays.equals(a,b))
			return true;
		else
			return false;
	}
	
	public void paintComponent(Graphics g) {
		try {
			BufferedImage background = ImageIO.read(new File("resources/img/background2.png/"));
			g.drawImage(background, 0, 0, getWidth(), getHeight(), null);
		} catch (IOException e) {}
	}
}
