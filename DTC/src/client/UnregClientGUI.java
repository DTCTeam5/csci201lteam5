package client;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class UnregClientGUI extends JFrame {

	private static final long serialVersionUID = -3704241293536602575L;
	private JPanel cardPanel;
	private CardLayout cardLayout;
	private UnregStartPanel startPanel;
	private UnregLoginPanel loginPanel;
	private UnregRegisterPanel registerPanel;
	private UnregFindPanel findPanel;
	
	UnregClientGUI(){
		initializeVariables();
		createGUI();
	}
	
	private void initializeVariables() {
		cardPanel = new JPanel();
		cardLayout = new CardLayout();
		startPanel = new UnregStartPanel();
		loginPanel = new UnregLoginPanel();
		registerPanel = new UnregRegisterPanel();
		findPanel = new UnregFindPanel();
	}
	
	private void createGUI() {
		cardPanel.setLayout(cardLayout);
		cardPanel.add(startPanel, "START");
		cardPanel.add(loginPanel, "LOGIN");
		cardPanel.add(registerPanel, "REGISTER");
		cardPanel.add(findPanel, "FIND");
		add(cardPanel, BorderLayout.CENTER);
		cardLayout.show(cardPanel, "START");
		this.setSize(Constants.unregGUIWidth, Constants.unregGUIHeight);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public void changeCard(String CARD) {
		cardLayout.show(cardPanel, CARD);
	}
}
