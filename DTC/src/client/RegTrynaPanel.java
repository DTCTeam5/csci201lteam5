package client;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import message.DTCColorUpdate;
import message.DTCUpdate;
import message.TrynaUpdate;

public class RegTrynaPanel extends JPanel{
	
	private static final long serialVersionUID = 1L;
	private JLabel nameLabel, DTCLabel;
	private JPanel namePanel, DTCPanel;
	private JTextField enterTrynaStatus;
	public RegTrynaPanel(){
		
		initializeComponents();
		createGUI();
		addAcationAdapter();
		
	}
	
	public void initializeComponents(){
	
		
		nameLabel = new JLabel(DTCClient.getClient().getMe().getUserName()+"     ");
		DTCLabel = new JLabel("   DTC   ");
		DTCPanel = new JPanel();
		namePanel = new JPanel();
	//	if()
		DTCPanel.setSize(100, this.getHeight());
		DTCPanel.setBackground(Color.green);
		namePanel.setSize(100, this.getHeight());
		
		namePanel.add(nameLabel);
		DTCPanel.add(DTCLabel);

		enterTrynaStatus = new JTextField(10);
		enterTrynaStatus.setText(DTCClient.getClient().getMe().getTrynaStatus());
		if(enterTrynaStatus.getText().equals("")) {
			enterTrynaStatus.setText("Please Enter What You Are Tryna Do Here");
		}
		enterTrynaStatus.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
				if(e.getKeyChar() == '\n' && !enterTrynaStatus.getText().isEmpty()) {
					TrynaUpdate trynaUpdate = new TrynaUpdate(DTCClient.getClient().getMe().getUserName(), enterTrynaStatus.getText());
					DTCClient.getClient().sendMessage(trynaUpdate);
				}
			}

			@Override
			public void keyPressed(KeyEvent e) {}

			@Override
			public void keyReleased(KeyEvent e) {}
		});
	}

	public void updateDTCStatus(){
		if(DTCPanel.getBackground()==Color.GREEN){
			//update dtc status
			DTCPanel.setBackground(Color.RED);
			DTCLabel.setText("Not DTC");
			DTCClient.getClient().sendMessage(new DTCColorUpdate(2));
			revalidate();
			repaint();
			
		}
		else if(DTCPanel.getBackground()==Color.RED){
			//update dtc status			
			DTCPanel.setBackground(Color.GREEN);
			DTCLabel.setText("DTC");
			DTCClient.getClient().sendMessage(new DTCColorUpdate(1));
			revalidate();
			repaint();
		}

	}
	public void addAcationAdapter(){
		DTCPanel.addMouseListener( new MouseAdapter(){
				public void mouseClicked(MouseEvent e){
						updateDTCStatus();
						
				}
				
				
			});
		
		}
	public void createGUI(){
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		add(namePanel);
		add(DTCPanel);
		add(Box.createHorizontalStrut(5));
		add(enterTrynaStatus);
		
	}
	
	public void updateTrynaText(String trynaMessage) {
		enterTrynaStatus.setText(trynaMessage);
	}
}
