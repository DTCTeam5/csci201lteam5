package client;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Vector;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import message.DTCUser;
import message.UpcommingPlan;

public class RegNewPlan extends JDialog {
	
	private static final long serialVersionUID = 1L;
	private JPanel mainPanel;
	private JTextField planName;
	private JTextField date;
	private JTextField time;
	private JTextField location;
	private JLabel newPlanLabel;
	private JLabel inviteFriendsLabel;
	private JList<RegListEntry> list;
	private DefaultListModel<RegListEntry> model;
	private JButton submit;
	private JScrollPane listScrollPane;
	
	public RegNewPlan(){
		super.setTitle("New Plan");
		initializeComponents();
		createGUI();
		addActions();
	}
	
	public void initializeComponents(){
		mainPanel = new JPanel();
		planName = new JTextField(10);
		date = new JTextField(10);
		time = new JTextField(10);
		location = new JTextField(10);
		newPlanLabel = new JLabel("New Plan");
		list = null;
		model = new DefaultListModel<RegListEntry>();
		submit = new JButton("Submit");
		inviteFriendsLabel = new JLabel("Invite Friends");
		
	}
	
	public void createGUI(){
		setLayout(new BorderLayout());
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		setSize(400, 400);
		setLocation(100, 100);
		
		
		for(DTCUser friend : DTCClient.getClient().getMe().getFriendsList()) {
			model.addElement(new RegListEntry(friend.getUserName(), friend.getProfilePicture()));
		}
		
		
		list = new JList<RegListEntry>(model);
		list.setCellRenderer(new RegListEntryCellRenderer());
		listScrollPane = new JScrollPane(list, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		
		mainPanel.add(newPlanLabel);
		mainPanel.add(Box.createVerticalStrut(8));
		mainPanel.add(planName);
		planName.setText("Plan Name");
		mainPanel.add(Box.createVerticalStrut(8));
		mainPanel.add(date);
		date.setText("Date");
		mainPanel.add(Box.createVerticalStrut(8));
		mainPanel.add(time);
		time.setText("Time");
		mainPanel.add(Box.createVerticalStrut(8));
		mainPanel.add(location);
		mainPanel.add(Box.createVerticalStrut(8));
		mainPanel.add(inviteFriendsLabel);
		mainPanel.add(Box.createVerticalStrut(8));
		location.setText("Location");
		add(mainPanel, BorderLayout.NORTH);
		add(listScrollPane, BorderLayout.CENTER);
		add(submit, BorderLayout.SOUTH);
		
		setVisible(true);
	}
	
	private void addActions(){
		submit.addActionListener(new ActionListener() {
			@Override
			/* UpcommingPlan(String title, String description, Timestamp time, String creatorUsername,
			Vector<String> inviteduserNames, String streetAddress, Vector<String> sendToFriends) */
			public void actionPerformed(ActionEvent e) {
				Calendar calendar = Calendar.getInstance();
				DTCUser me = DTCClient.getClient().getMe();
				Vector<String> usernamesToInvite = new Vector<String>();
				int[] selectedIx = list.getSelectedIndices();
				
				for(int i : selectedIx){
					usernamesToInvite.addElement(((RegListEntry) model.getElementAt(i)).getUsername());
				}
				
				UpcommingPlan newPlan = new UpcommingPlan(planName.getText(),
						"", 
						new Timestamp(calendar.getTime().getTime()), 
						me.getUserName(), 
						null, 
						location.getText(), 
						usernamesToInvite);
				DTCClient.getClient().sendMessage(newPlan);
				RegNewPlan.this.dispose();
			}
		});
	}
	
	public static void main(String [] args){
		new RegNewPlan();
	}
}