package client;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import message.UpcommingPlanResponse;

public class RegPlanRequestClicked extends JDialog {

	private static final long serialVersionUID = 6772079295333947238L;
	private JPanel topPanel;
	private JPanel centerPanel;
	private JPanel eastWithinCenter;
	private JPanel westWithinCenter;
	private JLabel planName;
	private JLabel dateAndTime;
	private JLabel location;
	private JButton joinPlanButton;
	private JButton rejectPlanButton;
	private String eventName;
	private String eventTime;
	private String eventLocation;
	private boolean accepted;
	private RegPlanRequests rpr;
	private RegPlanRequestClicked accessible;
	
	public RegPlanRequestClicked(String eventName, String eventTime, String eventLocation,RegPlanRequests rpr){
		this.eventName = eventName;
		this.eventTime = eventTime;
		this.eventLocation = eventLocation;
		this.rpr =rpr;
		accessible = this;
		initializeComponents();
		createGUI();
		addActionAdapter();
	}
	
	private void initializeComponents() {
		topPanel = new JPanel();
		topPanel.setLayout(new BoxLayout(topPanel, BoxLayout.X_AXIS));
		centerPanel = new JPanel();
		centerPanel.setLayout(new BorderLayout());
		eastWithinCenter = new JPanel();
		eastWithinCenter.setLayout(new GridLayout(8, 1));
		westWithinCenter = new JPanel();
		westWithinCenter.setLayout(new BorderLayout());
		planName = new JLabel(eventName);
		dateAndTime = new JLabel(eventTime);
		location = new JLabel(eventLocation);
		joinPlanButton = new JButton("Join Plan");
		rejectPlanButton = new JButton("Reject Plan");
		accepted = false;
	}
	
	private void createGUI(){
		setSize(400, 100);
		setLocation(100, 100);
		setLayout(new BorderLayout());
		topPanel.add(planName);
		topPanel.add(Box.createHorizontalStrut(5));
		topPanel.add(new JLabel("|"));
		topPanel.add(Box.createHorizontalStrut(5));
		topPanel.add(dateAndTime);
		topPanel.add(Box.createHorizontalStrut(5));
		topPanel.add(new JLabel("|"));
		topPanel.add(Box.createHorizontalStrut(5));
		topPanel.add(location);

		JPanel jp1 = new JPanel();
		jp1.setLayout(new BoxLayout(jp1, BoxLayout.X_AXIS));
		jp1.add(new JLabel("Created By: "));
		jp1.add(Box.createHorizontalStrut(5));
		jp1.add(new JLabel("|"));
		jp1.add(Box.createHorizontalStrut(5));
		jp1.add(new JLabel("Invited By: "));
		eastWithinCenter.add(jp1);
		eastWithinCenter.add(new JLabel("Going"));
		JPanel jp2 = new JPanel();
		jp2.setLayout(new GridLayout(3, 4));
		
		// add Images here for friends that are going
		eastWithinCenter.add(jp2);
		eastWithinCenter.add(new JLabel("Invited"));
		JPanel jp3 = new JPanel();
		jp3.setLayout(new GridLayout(3, 4));
		
		// add Images here for friends that are invited
		eastWithinCenter.add(new JLabel("Rejected"));
		JPanel jp4 = new JPanel();
		jp4.setLayout(new GridLayout(3, 4));
		
		// add Images here for friends that rejected the invitation
		JPanel planButtons = new JPanel();
		planButtons.setLayout(new BoxLayout(planButtons, BoxLayout.X_AXIS));
		planButtons.add(joinPlanButton);
		planButtons.add(rejectPlanButton);
		eastWithinCenter.add(planButtons);
		centerPanel.add(eastWithinCenter, BorderLayout.WEST);
		centerPanel.add(westWithinCenter, BorderLayout.EAST);
		add(topPanel, BorderLayout.NORTH);
		add(centerPanel, BorderLayout.CENTER);
		
		setVisible(true);
		setSize(new Dimension(400,400));
	}

	public void addActionAdapter() {
		joinPlanButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				UpcommingPlanResponse serverResponse = new UpcommingPlanResponse(rpr.getPlanID(), true);
				DTCClient.getClient().sendMessage(serverResponse);
				accessible.dispose();
			}
			
		});
		rejectPlanButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				UpcommingPlanResponse serverResponse = new UpcommingPlanResponse(rpr.getPlanID(), false);
				DTCClient.getClient().sendMessage(serverResponse);
				accessible.dispose();
			}			
		});
	}
}

