package client;

import javax.swing.ImageIcon;

public class RegListEntry {
	
	 private String username;
	 private ImageIcon icon;
	  
	   public RegListEntry(String value, ImageIcon icon) {
	      this.username = value;
	      this.icon = icon;
	   }
	  
	   public String getUsername() {
	      return username;
	   }
	  
	   public ImageIcon getIcon() {
	      return icon;
	   }
}
