package client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class RegAddFriendPanel extends JPanel{
	
	private static final long serialVersionUID = 1L;
	private JButton addFriend;
	public RegAddFriendPanel(){
		initializeComponents();
		createGUI();
		
	}
	
	public void initializeComponents(){
		addFriend = new JButton("+ Add Friend");
		addFriend.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				RegAddFriendDialog.getDialog().showFriendsList();
			}
		});
	}
	
	public void createGUI(){
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
		add(addFriend);
	}
}
