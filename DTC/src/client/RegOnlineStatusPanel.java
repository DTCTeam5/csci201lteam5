package client;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class RegOnlineStatusPanel extends JPanel{
	

	private static final long serialVersionUID = 1L;
	int isOnline;
	

	public RegOnlineStatusPanel(int status){
	
	isOnline = status;
	}
	
	
	protected void updateStatus(int status){
		isOnline = status;
		repaint();
	}

	protected void paintComponent(Graphics g) { 
	    int h = this.getHeight();
	    int w = this.getWidth();
	    super.paintComponent(g); 
	    if(isOnline == 1 || isOnline == 2){
	    	g.setColor(Color.GREEN);
	    }
	    else if(isOnline == 0 ){
	    	g.setColor(Color.RED);
	    }

	    g.fillOval(10, 10, h-15, h-15); 
	} 

}
