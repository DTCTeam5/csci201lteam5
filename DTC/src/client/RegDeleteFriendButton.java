package client;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;


public class RegDeleteFriendButton extends JButton{
	

	
	private static final long serialVersionUID = 1L;
	private BufferedImage myImage = null;
	//String text = null;

	RegDeleteFriendButton(String name){
		try {
			 myImage = ImageIO.read(new File("resources/img/red-01-1.jpg"));
	
		    } 
			catch (IOException e) {
		        e.printStackTrace();
		    }
			//text=name;
			//this.setText(name);
			this.setBorderPainted(false);
			this.setFocusable(false);
		}
	
	
	@Override
	protected void paintComponent(Graphics g) {  
		super.paintComponent(g);    
		setContentAreaFilled(false);
	    setOpaque(true);
	    g.drawImage(myImage, 0, 0, this.getWidth(), this.getHeight(), this);
	   // g.drawString(text, 20, 20);
	}
}
