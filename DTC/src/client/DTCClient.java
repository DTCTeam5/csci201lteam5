package client;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Properties;
//import com.apple.eawt.Application;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import message.ChatMessage;
import message.DTCColorUpdate;
import message.DTCUpdate;
import message.DTCUser;
import message.DataMessage;
import message.EmailSearch;
import message.FoundUser;
import message.FriendRequest;
import message.FriendRequestResponse;
import message.LoginInfo;
import message.PhoneSearch;
import message.RegisterFailure;
import message.RegisterInfo;
import message.RejectUpcomingPlan;
import message.TrynaUpdate;
import message.UsernameSearch;
import message.UpcommingPlan;
import message.UpcommingPlanResponse;
import message.LoginFailure;

public class DTCClient implements Runnable {
	private static DTCClient DTC;
	private DTCUser me;
	private boolean serverConnected;
	private Socket socket;
	private ObjectOutputStream oos;
	private ObjectInputStream ois;
	private BufferedImage dockIcon;
	private static UnregClientGUI unregGUI;
	private static RegClientGUI regGUI;
	private State state;
	enum State {UNREGISTERED, REGISTERED}
	
	static{
		DTC = new DTCClient();
		unregGUI = new UnregClientGUI();
	}
	
	public DTCClient() {
		connect();
		state = State.UNREGISTERED;
		me = null;
		setDockIcon();
	}
	
	private void connect() {
		FileInputStream input = null;
		Properties prop = new Properties();
		try {
			input = new FileInputStream(Constants.resourceFilepath + Constants.ClientConfigFilepath);
			prop.load(input);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String host = prop.getProperty("host");
		int port = Integer.parseInt(prop.getProperty("port"));
		Socket s;
		try {
			s = new Socket(host, port);
		} catch (IOException e) {
			s = null;
		}
		socket = s;
		if (socket != null){
			serverConnected = true;
			try {
				oos = new ObjectOutputStream(socket.getOutputStream());
				ois =  new ObjectInputStream(socket.getInputStream());
				new Thread(this).start();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			serverConnected = false;
			oos = null;
			ois = null;
		}
	}
	
	private void disconnect() {

		try {
			if(oos != null) {
				oos.close();
			}
			if(ois != null) {
				ois.close();
			}
			if(socket != null) {
				socket.close();
			}
		} catch (IOException e) {
			if(Constants.DEBUGMODE) {
				e.printStackTrace();
			}
		}
	}
	
	public static UnregClientGUI getUnregGUI() {
		return unregGUI;
	}
	
	public static RegClientGUI getRegGUI() {
		return regGUI;
	}
	
	public static DTCClient getClient() {
		return DTC;
	}
	
	public DTCUser getMe() {
		return me;
	}
	
	/* use this to send objects to the server
	 * also accessible because DTCClient is static
	 * not sure if returning a boolean will be helpful in the future
	 */
	public boolean sendMessage(DataMessage dm) {
		if(dm == null) return false;
		if(Constants.DEBUGMODE) {
			System.out.println("Sending: " + dm.toString());
		}
		
		try {
			oos.writeObject(dm);
			oos.flush();
			
			return true;
		} catch(IOException ioe) {
			if(Constants.DEBUGMODE) {
				ioe.printStackTrace();
			}
		}
		
		return false;
	}
	
	private void setDockIcon() {
		try {
			dockIcon = ImageIO.read(new File(Constants.iconFilepath));
		} catch (IOException e) {}
		//Application.getApplication().setDockIconImage(dockIcon);
	}
	
	public boolean isServerConnected() {
		return serverConnected;
	}
	
	private void launch(DTCUser loggedInUser) {
		state = State.REGISTERED;
		me = loggedInUser;
		unregGUI.setVisible(false);
		regGUI = new RegClientGUI();
	}
	
	public void register(String fname, String lname, String email, String phone, String username, String password, ImageIcon profilePicture) {
		if (socket.isInputShutdown() || socket.isOutputShutdown() || socket.isClosed() || !socket.isConnected()) {
			JOptionPane.showMessageDialog(unregGUI,
				    "Registration Failed. Cannot Connect to Server",
				    "Server Error",
				    JOptionPane.WARNING_MESSAGE);
		} else {
			RegisterInfo registerMessage = new RegisterInfo(fname, lname, email, phone, username, password, profilePicture);
			sendMessage(registerMessage);
		}
	}
	
	public void login(String username, String password) {
		if (socket.isInputShutdown() || socket.isOutputShutdown() || socket.isClosed() || !socket.isConnected()) {
			JOptionPane.showMessageDialog(unregGUI,
				    "Log In Failed. Cannot Connect to Server",
				    "Server Error",
				    JOptionPane.WARNING_MESSAGE);
		} else {
			LoginInfo loginMessage = new LoginInfo(username, password);
			sendMessage(loginMessage);
		}
	}
	
	public void usernameSearch(String username) {
		if (socket.isInputShutdown() || socket.isOutputShutdown() || socket.isClosed() || !socket.isConnected()) {
			JOptionPane.showMessageDialog(unregGUI,
				    "Log In Failed. Cannot Connect to Server",
				    "Server Error",
				    JOptionPane.WARNING_MESSAGE);
		} else {
			UsernameSearch usernameSearch = new UsernameSearch(username);
			sendMessage(usernameSearch);
		}
	}
	
	public void emailSearch(String email) {
		if (socket.isInputShutdown() || socket.isOutputShutdown() || socket.isClosed() || !socket.isConnected()) {
			JOptionPane.showMessageDialog(unregGUI,
				    "Log In Failed. Cannot Connect to Server",
				    "Server Error",
				    JOptionPane.WARNING_MESSAGE);
		} else {
			EmailSearch emailSearch = new EmailSearch(email);
			sendMessage(emailSearch);
		}
		
	}
	
	public void phoneSearch(String phone) {
		if (socket.isInputShutdown() || socket.isOutputShutdown() || socket.isClosed() || !socket.isConnected()) {
			JOptionPane.showMessageDialog(unregGUI,
				    "Log In Failed. Cannot Connect to Server",
				    "Server Error",
				    JOptionPane.WARNING_MESSAGE);
		} else {
			PhoneSearch phoneSearch = new PhoneSearch(phone);
			sendMessage(phoneSearch);
		}
	}

	@Override
	public void run() {
		try {
			while(socket != null && socket.isConnected()) {
				DataMessage incomingObject = null;
				try {
					incomingObject = (DataMessage)ois.readObject();
					System.out.println("Recieved: " + incomingObject.toString());
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				}
				switch(state) {
				case UNREGISTERED:
					if(incomingObject instanceof LoginFailure) {
						JOptionPane.showMessageDialog(unregGUI,
							    "Log In Failed, Incorrect Username of Password",
							    "Login Error",
							    JOptionPane.WARNING_MESSAGE);
					} else if(incomingObject instanceof RegisterFailure) {
						JOptionPane.showMessageDialog(unregGUI,
							    "Registration Failed, User already Exists",
							    "Registration Error",
							    JOptionPane.WARNING_MESSAGE);
					} else if(incomingObject instanceof DTCUser) {
						launch((DTCUser)incomingObject);
					} else if (incomingObject instanceof FoundUser) {
						if(((FoundUser) incomingObject).isFound()){
							JOptionPane.showMessageDialog(unregGUI, 
									"User Found: " + ((FoundUser) incomingObject).getfName() + " " + ((FoundUser) incomingObject).getlName(), 
									"User Found", 
									JOptionPane.INFORMATION_MESSAGE, 
									((FoundUser) incomingObject).getProfilePicture());
						}
						else {
							JOptionPane.showMessageDialog(unregGUI, 
									"User Not Found", 
									"Not Found", 
									JOptionPane.ERROR_MESSAGE);
						}
					}
				case REGISTERED:
					if(incomingObject instanceof FoundUser) {
						FoundUser user = (FoundUser)incomingObject;
						RegAddFriendDialog.getDialog().addSearchResult(user);
					}
					else if(incomingObject instanceof FriendRequest) { 
						FriendRequest friendRequest = (FriendRequest)incomingObject;
						regGUI.addFriendRequest(friendRequest);
					}
					else if(incomingObject instanceof FriendRequestResponse) {
						FriendRequestResponse friendRequestResponse = (FriendRequestResponse)incomingObject;
						if(friendRequestResponse.getResponse()) {
							DTCUser friend = friendRequestResponse.getUser();
							regGUI.addToFriendsList(friend);
						}
					}
					else if(incomingObject instanceof UpcommingPlan) {
						UpcommingPlan upcommingPlan = (UpcommingPlan)incomingObject;
						if(upcommingPlan.getAccepted()) {
							regGUI.addAcceptedPlan(upcommingPlan);
						}
						else {
							regGUI.addNotAcceptedPlan(upcommingPlan);
						}
					}
					else if(incomingObject instanceof DTCUpdate){
						DTCUpdate update = (DTCUpdate) incomingObject;
						regGUI.updateFriendStatus(update.getUsername(),update.getStatus());
					}
					else if(incomingObject instanceof UpcommingPlanResponse) {
						UpcommingPlanResponse upcommingPlanResponse = (UpcommingPlanResponse)incomingObject;
						
						regGUI.dealWithPlanResponse(upcommingPlanResponse);
					}
					else if(incomingObject instanceof RejectUpcomingPlan){
						RejectUpcomingPlan rejectUpcomingPlan = (RejectUpcomingPlan)incomingObject;
						
						if(rejectUpcomingPlan.getUsername() == me.getUserName()) {
							regGUI.dealWithRejectingUpcomingPlan(rejectUpcomingPlan);
						}
						else {
							regGUI.removeFriendFromAcceptedPlan(rejectUpcomingPlan);
						}
					}
					else if(incomingObject instanceof ChatMessage) {
						ChatMessage chatMessage = (ChatMessage)incomingObject;
						regGUI.updateChat(chatMessage);
					}
					else if(incomingObject instanceof TrynaUpdate) {
						TrynaUpdate trynaUpdate = (TrynaUpdate)incomingObject;
						regGUI.updateFriendTryna(trynaUpdate.getUsername(), trynaUpdate.getTrynaStatus());
					}
/*					else if(incomingObject instanceof DTCColorUpdate){
						DTCColorUpdate temp = (DTCColorUpdate)incomingObject;
						regGUI.updateDTCColor(temp.getDTCColor(),temp.getUsername());
					}*/
				}
			}
		} catch(IOException ioe){
			
		}
	}
	
	public static void main(String[] args) {
		
	}
}

