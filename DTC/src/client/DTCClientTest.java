/*
 * Jason: This is some useless test to ensure the server is working
 * Creates two buttons : Test Signup & Test Login
 * Creates an account root:root
 * Logs into an account root:root
 */

package client;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import constants.Constants;
import constants.SpringUtilities;
import message.DTCUser;
import message.DataMessage;
import message.FNameUpdate;
import message.FriendRequest;
import message.FriendRequestResponse;
import message.LNameUpdate;
import message.LoginInfo;
import message.PasswordUpdate;
import message.RegisterInfo;
import message.LoginFailure;
import message.UsernameSearch;

public class DTCClientTest extends JFrame implements Runnable {
	private static final long serialVersionUID = -7639793195932898775L;
	private Socket s;
	private ObjectOutputStream oos;
	private ObjectInputStream ois;
	private DTCUser currentUser;
	private String userName;
	private JPanel centerPanel;
	private JPanel friendsPanel;
	private JPanel searchPanel;
	private JPanel searchResultsPanel;
	private State state;
	
	private final String[] SIGNUPLABELS = { "Signup", "Username:", "Password:", "Email:", "First:", "Last:", "Phone#:" };
	private final String[] LOGINLABELS = { "Login", "Username:", "Password:" };
	enum Type { SIGNUP, LOGIN }
	enum State { IDLE, UNREGISTERED, LOGGINGIN, REGISTERED }
	
	public DTCClientTest() {
		super("DTCClientTest " + Constants.VERSION);
		setSize(600, 400);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setResizable(false);
		connect();
		createGUI();
		Box allButtons = Box.createVerticalBox();
		Box FnameUp = Box.createHorizontalBox();
		Box LnameUp = Box.createHorizontalBox();
		Box passwordUp = Box.createHorizontalBox();
		JButton passUpdate = new JButton("Update Password");
		JTextField passField = new JTextField(15); 
		JButton FnameUpdate = new JButton("Update First Name");
		JButton LnameUpdate = new JButton("Update Last Name");
		JTextField newLastName = new JTextField(15);
		JTextField newName = new JTextField(15);
		LnameUp.add(LnameUpdate);
		LnameUp.add(newLastName);
		FnameUp.add(FnameUpdate);
		FnameUp.add(newName);
		passwordUp.add(passUpdate);
		passwordUp.add(passField);
		allButtons.add(FnameUp);
		allButtons.add(LnameUp);
		allButtons.add(passwordUp);
		this.add(allButtons,BorderLayout.SOUTH);
		
		//Action Listener of the Password update
		passUpdate.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String newNameStr = passField.getText();
				if(!newNameStr.equals("")){
					PasswordUpdate pu = new PasswordUpdate(newNameStr);
					try {
						oos.writeObject(pu);
						oos.flush();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				}
				
			}
			
		});
	
		//Action Listener of the Last name update
		LnameUpdate.addActionListener(new ActionListener (){

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				String newNameStr = newLastName.getText();
				if(!newNameStr.equals("")){
					LNameUpdate lnu = new LNameUpdate(newNameStr);
					try {
						oos.writeObject(lnu);
						oos.flush();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				}
				
			}
			
		});
		
		
		
		//Action Listener of the First name update
		FnameUpdate.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
				String newNameStr = newName.getText();
				if(!newNameStr.equals("")){
					FNameUpdate fnu = new FNameUpdate(newNameStr);
					try {
						oos.writeObject(fnu);
						oos.flush();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
				}
				
			}
			
		});
		setVisible(true);
	}
	
	private void connect() {
		state = State.IDLE;
		userName = "";
		
		try {
			s = new Socket("localhost", 6789);
			oos = new ObjectOutputStream(s.getOutputStream());
			ois = new ObjectInputStream(s.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		state = State.UNREGISTERED;
		
		new Thread(this).start();
	}
	
	private void disconnect() {
		try {
			oos.close();
			ois.close();
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	private void createGUI() {
		centerPanel = new JPanel();
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
		
		centerPanel.add(createFormPanel(SIGNUPLABELS, Type.SIGNUP));
		centerPanel.add(createFormPanel(LOGINLABELS, Type.LOGIN));
		
		JScrollPane scrollPane = new JScrollPane(centerPanel);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		add(scrollPane, BorderLayout.CENTER);
		pack();
		
		friendsPanel = new JPanel();
		friendsPanel.setLayout(new BoxLayout(friendsPanel, BoxLayout.Y_AXIS));
		searchPanel = new JPanel();
		searchPanel.setLayout(new BoxLayout(searchPanel, BoxLayout.Y_AXIS));
		searchResultsPanel = new JPanel();
		searchResultsPanel.setLayout(new BoxLayout(searchResultsPanel, BoxLayout.Y_AXIS));
	}
	
	private void update() {
		validate();
		repaint();
		pack();
	}
	
	private JPanel createFormPanel(String[] labels, Type type) {
		JPanel main = new JPanel();
		main.setLayout(new BoxLayout(main, BoxLayout.Y_AXIS));
		JTextField textFields[] = new JTextField[labels.length - 1];
		JPanel form = new JPanel(new SpringLayout());
		main.setBorder(BorderFactory.createTitledBorder(labels[0]));
		for(int i = 0; i < labels.length - 1; i++) {
			JLabel label = new JLabel(labels[i + 1]);
			form.add(label);
			textFields[i] = new JTextField(15);
			label.setLabelFor(textFields[i]);
			form.add(textFields[i]);
		}
		
		SpringUtilities.makeCompactGrid(form, labels.length-1, 2, 6, 6, 6, 6);
		
		JPanel actionPanel = new JPanel();
		JButton actionButton = new JButton(labels[0]);
		switch(type) {
		case SIGNUP:
			actionButton.addActionListener(new SignupListener(textFields));
			break;
		case LOGIN:
			actionButton.addActionListener(new LoginListener(textFields));
			break;
		}
		actionPanel.add(actionButton);
		
		main.add(form);
		main.add(actionButton);
		
		return main;
	}
	
	private JPanel createStatusPanel(DTCUser user) {
		JPanel main = null;
		if(user == null)
			return main;
		
		main = new JPanel();
		main.setLayout(new BoxLayout(main, BoxLayout.Y_AXIS));
		String[] labels = { "Username:", "Email:", "First:", "Last:", "Phone#:" };
		JTextField textFields[] = new JTextField[labels.length];
		JPanel form = new JPanel(new SpringLayout());
		main.setBorder(BorderFactory.createTitledBorder(user.getUserName()));
		for(int i = 0; i < labels.length; i++) {
			JLabel label = new JLabel(labels[i]);
			form.add(label);
			textFields[i] = new JTextField(15);
			textFields[i].setEditable(false);
			label.setLabelFor(textFields[i]);
			form.add(textFields[i]);
		
				
		}
		
		textFields[0].setText(user.getUserName());
		textFields[1].setText(user.getEmail());
		textFields[2].setText(user.getfName());
		textFields[3].setText(user.getlName());
		textFields[4].setText(user.getPhoneNumber());
		
		SpringUtilities.makeCompactGrid(form, labels.length, 2, 6, 6, 6, 6);
		

		main.add(form);
		
		return main;
	}
	
	private JPanel createSearchPanel() {
		JPanel main = new JPanel();
		main.setLayout(new BoxLayout(main, BoxLayout.Y_AXIS));
		main.setBorder(BorderFactory.createTitledBorder("Search"));
		
		JPanel searchForm = new JPanel(new SpringLayout());
		JLabel searchLabel = new JLabel("Search:");
		JTextField searchField = new JTextField(15);
		searchLabel.setLabelFor(searchField);
		searchForm.add(searchLabel);
		searchForm.add(searchField);
		
		SpringUtilities.makeCompactGrid(searchForm, 1, 2, 6, 6, 6, 6);
		
		JButton searchButton = new JButton("Search");
		searchButton.addActionListener(new SearchListener(searchField));
		
		main.add(searchForm);
		main.add(searchButton);
		
		return main;
	}

	private JPanel createSearchResultsPanel(DTCUser userResults) {
		JPanel main = new JPanel();
		main.setLayout(new BoxLayout(main, BoxLayout.Y_AXIS));
		
		main.add(createSingleSearchResultPanel(userResults));
		
		return main;
	}
	
	private JPanel createSingleSearchResultPanel(DTCUser user) {
		JPanel main = new JPanel();
		main.setLayout(new BoxLayout(main, BoxLayout.X_AXIS));
		JLabel usernameLabel = new JLabel(user.getUserName());
		JButton actionButton = new JButton("Add");
		actionButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				FriendRequest friendRequest = new FriendRequest(userName, user.getUserName());
				
				try {
					oos.writeObject(friendRequest);
					oos.flush();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		
		main.add(usernameLabel);
		main.add(actionButton);
		
		return main;
	}
	
	private class SignupListener implements ActionListener {
		private JTextField[] textFields;
		private SignupListener(JTextField[] textFields) {
			this.textFields = textFields;
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			for(JTextField text : textFields) {
				if(text.getText().isEmpty()) {
					return;
				}
			}
			
			/*RegisterInfo registerInfo = new RegisterInfo(textFields[0].getText(), textFields[1].getText(), 
														 textFields[2].getText(), textFields[3].getText(),
														 textFields[4].getText(), Integer.parseInt(textFields[5].getText()));*/
			try {
				//oos.writeObject(registerInfo);
				oos.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private class LoginListener implements ActionListener {
		private JTextField[] textFields;
		private LoginListener(JTextField[] textFields) {
			this.textFields = textFields;
		}
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			for(JTextField text : textFields) {
				if(text.getText().isEmpty()) {
					return;
				}
			}
			
			userName = textFields[1].getText();
			LoginInfo loginInfo = new LoginInfo(textFields[0].getText(), textFields[1].getText());
			try {
				oos.writeObject(loginInfo);
				oos.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private class SearchListener implements ActionListener {
		private JTextField text;
		private SearchListener(JTextField text) {
			this.text = text;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			searchResultsPanel.removeAll();
			update();
			if(text.getText().isEmpty()) {
				return;
			}
			if(text.getText().equals(userName)) {
				JOptionPane.showMessageDialog(DTCClientTest.this, "You played yourself.");
				return;
			}
			for(DTCUser friend : currentUser.getFriendsList()) {
				if(text.getText().equals(friend.getUserName())) {
					JOptionPane.showMessageDialog(DTCClientTest.this, "Already added friend.");
					return;
				}
			}
			
			UsernameSearch usernameSearch = new UsernameSearch(text.getText());
			try {
				oos.writeObject(usernameSearch);
				oos.flush();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	@Override
	public void run() {
		try {
			while(s != null && s.isConnected()) {
				DataMessage incomingObject = null;
				try {
					incomingObject = (DataMessage)ois.readObject();
					System.out.println("Recieved: " + incomingObject.toString());
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				}
				
				switch(state) {
				case UNREGISTERED:
					if(incomingObject instanceof LoginFailure) {
						LoginFailure response = (LoginFailure)incomingObject;
						JOptionPane.showMessageDialog(this, "Server: ");
					}
					else if(incomingObject instanceof DTCUser) {
						currentUser = (DTCUser)incomingObject;
						
						centerPanel.removeAll();
						
						friendsPanel.add(createStatusPanel(currentUser));
						for(DTCUser friend : currentUser.getFriendsList()) {
							friendsPanel.add(createStatusPanel(friend));
						}
						
						searchPanel.add(createSearchPanel());
						

						centerPanel.add(friendsPanel);
						centerPanel.add(searchPanel);
						centerPanel.add(searchResultsPanel);
						update();
						
						state = State.REGISTERED;
					}
					break;
				case REGISTERED:
					if(incomingObject instanceof DTCUser) {
						DTCUser searchedUser = (DTCUser)incomingObject;
						
						searchResultsPanel.add(createSearchResultsPanel(searchedUser));
						
						update();
					}
					else if(incomingObject instanceof FriendRequest) {
						FriendRequest friendRequest = (FriendRequest)incomingObject;
						FriendRequestResponse friendRequestResponse;
						
						if(0 == JOptionPane.showOptionDialog(this, friendRequest.getFromID() + " wants to add you.", "Friend Request", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null)) {
							friendRequestResponse = new FriendRequestResponse(friendRequest, true);
						}
						else {
							friendRequestResponse = new FriendRequestResponse(friendRequest, false);
						}
						
						try {
							oos.writeObject(friendRequestResponse);
							oos.flush();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					else if(incomingObject instanceof FriendRequestResponse) {
						FriendRequestResponse friendRequestResponse = (FriendRequestResponse)incomingObject;
						
						if(friendRequestResponse.getResponse()) {
							friendsPanel.add(createStatusPanel(friendRequestResponse.getUser()));
							update();
						}
					}
					break;
				case IDLE:
					break;
				default:
					break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
			JOptionPane.showConfirmDialog(this, "Cannot connect to the server.\nClosing");
		} finally {
			disconnect();
		}
	}
	
	public static void main(String[] args) {
		new DTCClientTest();
	}
}
