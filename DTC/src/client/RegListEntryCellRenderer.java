package client;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

public class RegListEntryCellRenderer extends JLabel implements ListCellRenderer {
	
	private static final long serialVersionUID = 1L;
	  
	   public Component getListCellRendererComponent(JList list, Object value,
	                                                 int index, boolean isSelected,
	                                                 boolean cellHasFocus) {
	      RegListEntry entry = (RegListEntry)value;
	  
	      setText(entry.getUsername());
	      setIcon(entry.getIcon());
	   
	      if (isSelected) {
	         setBackground(list.getSelectionBackground());
	         setForeground(list.getSelectionForeground());
	      }
	      else {
	         setBackground(list.getBackground());
	         setForeground(list.getForeground());
	      }
	  
	      setEnabled(list.isEnabled());
	      setFont(list.getFont());
	      setOpaque(true);
	  
	      return this;
	   }
}
