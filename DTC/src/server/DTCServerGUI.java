package server;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import constants.Constants;

public class DTCServerGUI extends JFrame {
	private static final long serialVersionUID = 7184564127731885380L;

	private boolean started;
	private ServerListener scm;
	private JTextArea textArea;
	private JButton actionButton;
	private JScrollPane scrollPane;
	public DTCServerGUI() {
		super("DTCServer " + Constants.VERSION);
		setSize(450, 300);
		
		initVariables();
		addActionAdapter();
		createGUI();
	}
	
	private void initVariables() {
		started = false;
		textArea = new JTextArea();
		textArea.setEditable(false);
		actionButton = new JButton("Start");
		scrollPane = new JScrollPane(textArea);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
	}
	
	private void addActionAdapter() {
		this.addWindowListener(new WindowListener() {
			@Override
			public void windowClosing(WindowEvent e) {
				scm.shutDown();
				dispose();
			}
			
			@Override
			public void windowOpened(WindowEvent e) {}
			@Override
			public void windowClosed(WindowEvent e) {}
			@Override
			public void windowIconified(WindowEvent e) {}
			@Override
			public void windowDeiconified(WindowEvent e) {}
			@Override
			public void windowActivated(WindowEvent e) {}
			@Override
			public void windowDeactivated(WindowEvent e) {}
			
		});
		
		actionButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(scm == null)
					return;
				
				if(started) {
					actionButton.setText("Start");
					scm.shutDown();
				}
				else {
					actionButton.setText("Stop");
					Thread connectionThread = new Thread(scm);
					connectionThread.start();
				}
				
				started = !started;
			}
		});
	}
	
	private void createGUI() {
		this.add(scrollPane, BorderLayout.CENTER);
		this.add(actionButton, BorderLayout.SOUTH);
		
		setVisible(true);
	}
	
	public void sendMessage(String message) {
		textArea.append(message + "\n");
		textArea.setCaretPosition(textArea.getDocument().getLength());
	}

	public void setServerListener(ServerListener scm) {
		this.scm = scm;
	}
}
