package server;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Vector;

import javax.swing.ImageIcon;

import constants.Constants;
import message.ChatMessage;
import message.DTCColorUpdate;
import message.DTCUpdate;
import message.DTCUser;
import message.DataMessage;
import message.EmailSearch;
import message.FNameUpdate;
import message.FoundUser;
import message.FriendRequest;
import message.FriendRequestResponse;
import message.LNameUpdate;
import message.LoginInfo;
import message.PasswordUpdate;
import message.PhoneSearch;
import message.RegisterFailure;
import message.RegisterInfo;
import message.RejectUpcomingPlan;
import message.TrynaUpdate;
import message.UpcommingPlan;
import message.UpcommingPlanResponse;
import message.LoginFailure;
import message.UsernameSearch;

public class ServerConnectionManager extends Thread {
	private DTCServerGUI dsg;
	private ServerListener parentThread;
	private ObjectOutputStream oos;
	private ObjectInputStream ois;
	private Socket s;
	private String userName;
	private int userID;
	private int state;
	private Connection conn;
	public ServerConnectionManager(Socket s, DTCServerGUI dsg, ServerListener parentThread) {
		try {
			Class.forName(Constants.DATABASEDRIVER);
			conn = DriverManager.getConnection(Constants.DATABASELOGIN);
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		this.dsg = dsg;
		this.parentThread = parentThread;
		state = Constants.STATEIDLE;
		this.s = s;
		
		try {
			oos = new ObjectOutputStream(s.getOutputStream());
			ois = new ObjectInputStream(s.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		state = Constants.STATEUNREGISTERED;
		
		Thread thread = new Thread(this);
		thread.start();
	}
	
	String getUsername() {
		return userName;
	}
	
	void putOnHashMap() {
		parentThread.putOnHashMap(userName, this);
	}
	
	@Override
	public void run() {
		try {
			while(s.isConnected()) {
				DataMessage incomingObject = null;
				try {
					incomingObject = (DataMessage)ois.readObject();
				} catch (ClassNotFoundException e1) {
					e1.printStackTrace();
				} 
				
				try {
					switch(state) {
					case Constants.STATEIDLE:
						Thread.yield();
						break;
					case Constants.STATEUNREGISTERED:
						if(incomingObject instanceof UsernameSearch) {
							dsg.sendMessage("Recieved UsernameSearch");
							UsernameSearch usernameSearch = (UsernameSearch)incomingObject;
							searchUsername(usernameSearch.getUsername());
						}//
						else if(incomingObject instanceof EmailSearch) {
							dsg.sendMessage("Recieved EmailSearch");
							EmailSearch emailSearch = (EmailSearch)incomingObject;
							searchEmail(emailSearch.getEmail());
						}
						else if(incomingObject instanceof PhoneSearch) {
							dsg.sendMessage("Recieved PhoneSearch");
							PhoneSearch phoneSearch = (PhoneSearch)incomingObject;
							searchPhone(phoneSearch.getPhoneNumber());
						} //
						else if(incomingObject instanceof FriendRequest) {
							dsg.sendMessage("Recieved FriendRequest");
							FriendRequest friendRequest = (FriendRequest)incomingObject;
							parentThread.sendToThreads(friendRequest);
						}
						else if(incomingObject instanceof LoginInfo) {
							dsg.sendMessage("Recieved LoginInfo");
							LoginInfo loginInfo = (LoginInfo)incomingObject;
							
							if(attemptLogin(loginInfo)) {
								sendUserData(userName);
								updateOnlineStatus(1);
							}
							else {
								sendLoginFailure();
							}
						}
						else if(incomingObject instanceof RegisterInfo) {
							dsg.sendMessage("Recieved RegisterInfo");
							RegisterInfo registerInfo = (RegisterInfo)incomingObject;
							
							if(attemptRegister(registerInfo)) {
								sendUserData(registerInfo.getUserName());
							}
							else {
								sendRegisterFailure();
							}
						}
						break;
					case Constants.STATEREGISTERED:
						if(incomingObject instanceof UsernameSearch) {
							dsg.sendMessage("Recieved UsernameSearch");
							UsernameSearch usernameSearch = (UsernameSearch)incomingObject;
							
							searchUsername(usernameSearch.getUsername());
						}
						else if(incomingObject instanceof FriendRequest) {
							dsg.sendMessage("Recieved FriendRequest");
							FriendRequest friendRequest = (FriendRequest)incomingObject;
							PreparedStatement ps = conn.prepareStatement("SELECT * FROM FriendRequests WHERE fromUserID=(SELECT userID FROM Users WHERE username=?) AND toUserID=(SELECT userID FROM Users WHERE username=?)");
							ps.setString(1, friendRequest.getFromID());
							ps.setString(2, friendRequest.getToID());
							ResultSet rs = ps.executeQuery();
							if(!rs.next()){
								parentThread.sendToThreads(friendRequest);
							}
						}
						else if(incomingObject instanceof FriendRequestResponse) {
							dsg.sendMessage("Recieved FriendRequestResponse");
							FriendRequestResponse friendRequestResponse = (FriendRequestResponse)incomingObject;
							if(friendRequestResponse.getResponse()) {
								addFriend(friendRequestResponse.getFromID());
								
								friendRequestResponse.setUser(getDTCUser(friendRequestResponse.getToID()));
								FriendRequestResponse returnResponse = new FriendRequestResponse(friendRequestResponse.getToID(), friendRequestResponse.getFromID(), true);
								returnResponse.setUser(getDTCUser(friendRequestResponse.getFromID()));
								sendToClient(returnResponse, "FriendRequestResponse");
								parentThread.sendToThreads(friendRequestResponse);
							}
							deleteFriendRequest(friendRequestResponse);
						}
						else if(incomingObject instanceof FNameUpdate) {
							dsg.sendMessage("Recieved: FNameUpdate");
							FNameUpdate temp = (FNameUpdate) incomingObject;
							boolean b = sendFNameUpdate(temp.getFname());
							if(b) {
								dsg.sendMessage("First name successfully Updated.");
							} else {
								dsg.sendMessage("They don't want us to change the first name.");
							}
							parentThread.sendToThreads(temp);
						}
						else if (incomingObject instanceof LNameUpdate ){
							dsg.sendMessage("Recieved: LNameUpdate");
							LNameUpdate temp = (LNameUpdate) incomingObject;
							boolean b = sendLNameUpdate(temp.getLname());
							if(b) {
								dsg.sendMessage("Last name successfully Updated.");
							} else {
								dsg.sendMessage("They don't want us to change the last name.");
							}
							parentThread.sendToThreads(temp);
						}
						else if(incomingObject instanceof PasswordUpdate) {
							dsg.sendMessage("Recieved: PasswordUpdate");
							PasswordUpdate temp = (PasswordUpdate) incomingObject;
							boolean b = sendPasswordUpdate(temp.getPassword());
							if(b) {
								dsg.sendMessage("Password successfully Updated.");
							} else {
								dsg.sendMessage("They don't want us to change the password.");
							}
							parentThread.sendToThreads(temp);
						}
						else if(incomingObject instanceof PhoneSearch) {
							dsg.sendMessage("Recieved: PhoneSearch");
							PhoneSearch phoneSearch = (PhoneSearch)incomingObject;
							searchPhone(phoneSearch.getPhoneNumber());
							
							parentThread.sendToThreads(phoneSearch);
						}
						else if(incomingObject instanceof EmailSearch) {
							dsg.sendMessage("Recieved: EmailSearch");
							EmailSearch emailSearch = (EmailSearch)incomingObject;
							searchEmail(emailSearch.getEmail());

							parentThread.sendToThreads(emailSearch);
						}
						else if(incomingObject instanceof TrynaUpdate) {
							dsg.sendMessage("Recieved TrynaUpdate");
							TrynaUpdate trynaUpdate = (TrynaUpdate)incomingObject;
							updateTryna(trynaUpdate);
							
							parentThread.sendToThreads(trynaUpdate);
						}
						else if(incomingObject instanceof UpcommingPlan) {
							dsg.sendMessage("Recieved: UpcommingPlan");
							UpcommingPlan upcommingPlan = (UpcommingPlan)incomingObject;
							if(attemptMakePlan(upcommingPlan)) {
								dsg.sendMessage("Created plan: " + upcommingPlan.getTitle());
								if(upcommingPlan.getCreatorUsername().equals(userName)) {
									createPlanUsers(upcommingPlan);
									upcommingPlan.setAccepted(true);
									sendToClient(upcommingPlan, "UpcommingPlan");
									upcommingPlan.setAccepted(false);
									parentThread.sendToThreads(upcommingPlan);
								}
							}
							else {
								dsg.sendMessage("Failed creating plan: " + upcommingPlan.getTitle());
							}
						}
						else if(incomingObject instanceof DTCColorUpdate){
							dsg.sendMessage("Recieved: DTCColorUpdate");
							DTCColorUpdate temp = (DTCColorUpdate)incomingObject;
							updateStatus(temp.getDTCColor());
						}
						else if(incomingObject instanceof UpcommingPlanResponse) {
							dsg.sendMessage("Recieved: UpcommingPlanResponse");
							UpcommingPlanResponse upcommingPlanResponse = (UpcommingPlanResponse)incomingObject;
							if(upcommingPlanResponse.getResponse()) {
								acceptPlan(upcommingPlanResponse.getPlanID());
							}
							else {
								rejctPlan(upcommingPlanResponse.getPlanID());
							}
							upcommingPlanResponse.setPlan(getPlan(upcommingPlanResponse.getPlanID()));
							parentThread.sendToThreads(upcommingPlanResponse);
						}
						
						else if(incomingObject instanceof RejectUpcomingPlan){
							dsg.sendMessage("Recieved: RejectUpcomingPlan");
							RejectUpcomingPlan rup = (RejectUpcomingPlan)incomingObject;
							leavePlan(rup.getPlanID());
							sendToClient(rup, "RejectUpcomingPlan");
							parentThread.sendToThreads(getPlan(rup.getPlanID()));
						}
						else if(incomingObject instanceof ChatMessage) {
							dsg.sendMessage("Recieved: ChatMessage");
							ChatMessage chatMessage = (ChatMessage)incomingObject;
							parentThread.sendToThreads(chatMessage);
						}
						
						break;
					}
				} catch (SQLException e) {
					e.printStackTrace();
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			System.out.println("Server Connection Manager: Connection Closed");
			disconnect();
		}
	}
	
	private void leavePlan(int planID) {
		try {
			PreparedStatement ps = conn.prepareStatement(Constants.DATABASELEAVEPLAN);
			ps.setInt(1, planID);
			ps.setString(2, userName);
			ps.executeUpdate();
		} catch(SQLException e) {
			if(Constants.DEBUGMODE) {
				e.printStackTrace();
			}
		}
	}
	
	private void acceptPlan(int planID) {
		try {
			PreparedStatement ps = conn.prepareStatement(Constants.DTABASEACCEPTPLAN);
			ps.setString(1, userName);
			ps.setInt(2, planID);
			ps.executeUpdate();
		} catch(SQLException e) {
			if(Constants.DEBUGMODE) {
				e.printStackTrace();
			}
		}
	}
	
	private void rejctPlan(int planID) {
		try {
			PreparedStatement ps = conn.prepareStatement(Constants.DATABASEREJECTPLAN);
			ps.setInt(1, planID);
			ps.setString(2, userName);
			ps.executeUpdate();			
		} catch(SQLException e) {
			if(Constants.DEBUGMODE) {
				e.printStackTrace();
			}
		}
	}

	private void createPlanUsers(UpcommingPlan upcommingPlan) {
		try {
			// add creator
			createPlanUser(upcommingPlan.getPlanID(), upcommingPlan.getCreatorUsername(), 1, 1);
	
			// add invited friends
			for(String friend : upcommingPlan.getSendToFriends()) {
				createPlanUser(upcommingPlan.getPlanID(), friend, 0, 0);
			}
		} catch(SQLException e) {
			if(Constants.DEBUGMODE) {
				e.printStackTrace();
			}
		}
	}
	
	private void createPlanUser(int planID, String friendUsername, int accepted, int creator) throws SQLException {
		PreparedStatement ps = conn.prepareStatement(Constants.DATABASEADDPLANREQUEST);
		
		ps.setInt(1, planID);
		ps.setString(2, friendUsername);
		ps.setInt(3, accepted);
		ps.setInt(4, creator);
		
		ps.executeUpdate();
	}

	private void updateOnlineStatus(int i) {
		try {
			PreparedStatement ps = conn.prepareStatement(Constants.DATABASEGETONLINEFRIENDS);
			ps.setString(1, userName);
			ResultSet rs = ps.executeQuery();
			while(rs.next()){
				//int status = rs.getInt("dtcstatus");
				String username = rs.getString("username");
				DTCUpdate dtcu = new DTCUpdate(i, userName, username);
				parentThread.sendToThreads(dtcu);
			}
		} catch (SQLException e) {
			if(Constants.DEBUGMODE)
				e.printStackTrace();
		}
	}

	private void deleteFriendRequest(FriendRequestResponse friendRequestResponse) {
		try {
			PreparedStatement ps = conn.prepareStatement(Constants.DATABASEREMOVEFRIENDREQUEST);
			ps.setString(1, friendRequestResponse.getFromID());
			ps.setString(2, friendRequestResponse.getToID());
			ps.executeUpdate();
		} catch (SQLException e) {
			if(Constants.DEBUGMODE)
				e.printStackTrace();
		}
	}

	void disconnect() {
		updateOnlineStatus(0);
		
		try {
			updateStatus(Constants.STATUSOFFLINE);
			
			oos.close();
			ois.close();
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private boolean updateStatus(int newStatus) {
		try {
			PreparedStatement ps = conn.prepareStatement(Constants.DATABASESTATUS);
			ps.setInt(1, newStatus);
			ps.setString(2, userName);
			ps.executeUpdate();
			updateOnlineStatus(newStatus);
			
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	private boolean sendPasswordUpdate(String newPassword)throws  ClassNotFoundException, IOException{
		try{
			PreparedStatement ps = conn.prepareStatement(Constants.DATABASEUPDATEPASSWORD);
			
			ps.setString(1, Constants.hashPass(newPassword));
			ps.setInt(2, userID);
			ps.executeUpdate();
			return true;
			}catch(SQLException sqle){
				return false;
			}
	}
	private boolean sendLNameUpdate(String newLName) throws  ClassNotFoundException, IOException {
		try{
			PreparedStatement ps = conn.prepareStatement(Constants.DATABASEUPDATELNAME);
			ps.setString(1, newLName);
			ps.setInt(2, userID);
			ps.executeUpdate();
			return true;
			}catch(SQLException sqle){
				return false;
			}

	}
	
	private boolean sendFNameUpdate(String newName) throws  ClassNotFoundException, IOException {
		try{
		PreparedStatement ps = conn.prepareStatement(Constants.DATABASEUPDATEFNAME);
		ps.setString(1, newName);
		ps.setInt(2, userID);
		ps.executeUpdate();
		return true;
		}catch(SQLException sqle){
			return false;
		}
	}
	
	private void addFriend(String toUsername) {
		PreparedStatement ps;
		try {
			ps = conn.prepareStatement(Constants.DATABASEISFRIEND);
			ps.setInt(1, userID);
			ps.setString(2, toUsername);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {}
			else {
				ps = conn.prepareStatement(Constants.DATABASEADDFRIEND);
				ps.setString(1, userName);
				ps.setString(2, toUsername);
				ps.executeUpdate();
				
				ps.setString(1, toUsername);
				ps.setString(2, userName);
				ps.executeUpdate();
		} 
	
		}
		catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void sendLoginFailure() {
		LoginFailure loginFailure = new LoginFailure();
		sendToClient(loginFailure, "LoginFailure");
	}
	
	private void sendRegisterFailure() {
		RegisterFailure registerFailure = new RegisterFailure();
		sendToClient(registerFailure, "RegisterFailure");
	}
	
	private boolean attemptLogin(LoginInfo loginInfo) throws ClassNotFoundException, SQLException {		
		PreparedStatement ps = conn.prepareStatement(Constants.DATABASEGETUSERPASS);
		ps.setString(1, loginInfo.getUserName());
		ps.setString(2, loginInfo.getPassword());
		ResultSet rs = ps.executeQuery();
		dsg.sendMessage("Login-in attempt User:" + loginInfo.getUserName() + " Pass:" + loginInfo.getPassword());
		if(rs.next()) {
			// username with password given found
			userID = rs.getInt("userID");
			userName = rs.getString("userName");
			dsg.sendMessage("Login-in success User:" + userName);
			state = Constants.STATEREGISTERED;
			return true;
		}
		else {
			// username with password not found
			dsg.sendMessage("Login-in failure User:" + loginInfo.getUserName());
			return false;
		}
		
	}
	private void sendNotification() throws SQLException {
		sendFriendRequest();
		sendPlanRequests();
	}

	private void sendFriendRequest() throws SQLException {
		PreparedStatement ps = conn.prepareStatement(Constants.DATABASEGETFRIENDREQUEST);
		ps.setString(1, userName);
		ResultSet rs = ps.executeQuery();
		
		while(rs.next()) {
			FriendRequest friendRequest = new FriendRequest(rs.getString("username"), userName);
			sendToClient(friendRequest, "FriendRequest");
		}
	}
	
	private void sendPlanRequests() throws SQLException {
		PreparedStatement ps = conn.prepareStatement(Constants.DATABASEGETUSERPLANS);
		ps.setString(1, userName);
		ResultSet rs = ps.executeQuery();
		
		/*(String title, String description, Timestamp time, String creatorUsername,
			Vector<String> inviteduserNames, String streetAddress, Vector<String> sendToFriends)*/
		while(rs.next()) {
			UpcommingPlan newPlan = getPlan(rs.getInt("planID"));
			newPlan.setAccepted(rs.getBoolean("accepted"));
			
			this.sendToClient(newPlan, "UpcommingPlan");
		}
	}
	
	private UpcommingPlan getPlan(int planID) {
		UpcommingPlan plan = null;
		try {
			Vector<String> acceptedUsers = new Vector<String>();
			Vector<String> invitedUsers = new Vector<String>();
			PreparedStatement ps = conn.prepareStatement(Constants.DATABASEGETUSERSINVOLVEDINPLAN);
			ps.setInt(1, planID);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				if(rs.getBoolean("accepted")) {
					acceptedUsers.add(rs.getString("username"));
				}
				invitedUsers.add(rs.getString("username"));
			}
			
			ps = conn.prepareStatement(Constants.DATABASEGETPLAN);
			ps.setInt(1, planID);
			rs = ps.executeQuery();
			if(rs.next()) {
				plan = new UpcommingPlan(rs.getString("title"), rs.getString("description"), rs.getTimestamp("plantime"), rs.getString("username"), invitedUsers, rs.getString("address"), null);
				plan.setPlanID(planID);
				plan.setGoingFriends(acceptedUsers);
			}
		} catch(SQLException e) {
			if(Constants.DEBUGMODE)
				e.printStackTrace();
		}
		
		return plan;
	}
	
	private Vector<String> getInvitedFriends(int planID) throws SQLException {
		Vector<String> invitedFriends = new Vector<String>();
		PreparedStatement ps = conn.prepareStatement(Constants.DATABASEGETUSERSINVOLVEDINPLAN);
		ps.setInt(1, planID);
		ResultSet rs = ps.executeQuery();
		
		while(rs.next()) {
			invitedFriends.addElement(rs.getString("username"));
		}
		
		
		return invitedFriends;
	}

	private boolean attemptRegister(RegisterInfo regInfo) throws ClassNotFoundException, SQLException {
		PreparedStatement ps = conn.prepareStatement(Constants.DATABASEGETUSER);
		ps.setString(1, regInfo.getUserName());
		ResultSet rs = ps.executeQuery();
		dsg.sendMessage("Sign-up attempt User:" + regInfo.getUserName() + " Pass:" + regInfo.getPassword());
		if(rs.next()) {
			// Username already exists
			dsg.sendMessage("Sign-up failure User: " + regInfo.getUserName());
		}
		else {
			// Username doesnt exist so create one
			try {
				ps = conn.prepareStatement(Constants.DATABASEREGISTER);
				ps.setString(1, regInfo.getUserName());
				ps.setString(2, regInfo.getPassword());
				ps.setString(3, regInfo.getEmail());
				ps.setString(4, regInfo.getfName());
				ps.setString(5, regInfo.getlName());
				ps.setString(6, regInfo.getPhoneNumber());
				ps.executeUpdate();
				
				return attemptLogin(new LoginInfo(regInfo));
			} catch(SQLException sqle) {
				sqle.printStackTrace();
				return false;
			}
		}
		
	
		
		
		return false;
	}
	

	private boolean attemptMakePlan(UpcommingPlan upcommingPlan) {
		try {
		// creating the plan
			PreparedStatement ps = conn.prepareStatement(Constants.DATABASECREATEPLAN, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, upcommingPlan.getCreatorUsername());
			ps.setTimestamp(2, upcommingPlan.getTime());
			ps.setString(3, upcommingPlan.getTitle());
			ps.setString(4, upcommingPlan.getDescription());
			ps.setString(5, upcommingPlan.getStreetAddress());
			ps.execute();
			ResultSet rs = ps.getGeneratedKeys();
			
			if(rs.next()) {
				upcommingPlan.setPlanID(rs.getInt(1));
			}
			
			if(upcommingPlan.getInviteduserNames() == null) {
				upcommingPlan.setInviteduserNames(new Vector<String>());
			}
//			getPlanIDFromPlan(upcommingPlan);
			return true;
		} catch (SQLException e) {
			if(Constants.DEBUGMODE) {
				e.printStackTrace();
			}
			return false;
		}
	}

	// ONLY USED WHEN LOGGING IN
	private void sendUserData(String userName) throws SQLException, ClassNotFoundException, IOException {
		putOnHashMap();
		
		Vector<DTCUser> friendsList = new Vector<DTCUser>();
		PreparedStatement ps = conn.prepareStatement(Constants.DATABASEGETFRIENDS);
		ps.setInt(1, userID);
		ResultSet rs = ps.executeQuery();
		while(rs.next()) {
			DTCUser newFriend = new DTCUser(rs.getString("username"), rs.getString("email"), rs.getString("fname"), rs.getString("lname"), rs.getString("phoneNumber"), rs.getInt("dtcstatus"), rs.getString("trynastatus"), null, null);
			friendsList.add(newFriend);
		}
		
		ps = conn.prepareStatement(Constants.DATABASEGETUSER);
		ps.setString(1, userName);
		rs = ps.executeQuery();
		if(rs.next()) {
			// Username exists
			DTCUser user = new DTCUser(userName, rs.getString("email"), rs.getString("fname"), rs.getString("lname"), rs.getString("phoneNumber"), rs.getInt("dtcstatus"), rs.getString("trynastatus"), null, friendsList);
			sendToClient(user, "DTCUser: " + userName);
			updateStatus(Constants.STATUSONLINE);
			sendNotification();
			state = Constants.STATEREGISTERED;
		}
		else {
			return;
		}
	}
	
	private void searchPhone(String phoneNumber) {
		try {
			FoundUser user = null;
			PreparedStatement ps = conn.prepareStatement(Constants.DATABASEGETPHONE);
			ps.setString(1, phoneNumber);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				user = new FoundUser(true, rs.getString("username"), rs.getString("fname"), rs.getString("lname"), null);
			} else {
				user = new FoundUser(false, null, null, null, null);
			}
			sendToClient(user, "FoundUser");
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void searchEmail(String email) {
		try {
			FoundUser user = null;
			PreparedStatement ps = conn.prepareStatement(Constants.DATABASEGETEMAIL);
			ps.setString(1, email);
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()) {
				user = new FoundUser(true, rs.getString("username"), rs.getString("fname"), rs.getString("lname"), null);
			} else {
				user = new FoundUser(false, null, null, null, null);
			}
			sendToClient(user, "FoundUser");
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
	}
	
	private void searchUsername(String username) throws SQLException, ClassNotFoundException, IOException {
		FoundUser user = null;
		PreparedStatement ps = conn.prepareStatement(Constants.DATABASEGETUSER);
		ps.setString(1, username);
		ResultSet rs = ps.executeQuery();
		if(rs.next()) {
			user = new FoundUser(true, username, rs.getString("fname"), rs.getString("lname"), null);
		}
		else {
			user = new FoundUser(false, null, null, null, null);
		}
		sendToClient(user, "FoundUser");
	}

	void sendToClient(Object data, String message) {
		if(data == null)
			return;
		
		try {
			dsg.sendMessage("Sending " + message);
			oos.writeObject(data);
			oos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private DTCUser getDTCUser(String username) throws SQLException {
		DTCUser user = null;
		PreparedStatement ps = conn.prepareStatement(Constants.DATABASEGETUSER);
		ps.setString(1, username);
		ResultSet rs = ps.executeQuery();
		if(rs.next()) {
			user = new DTCUser(username, rs.getString("email"), rs.getString("fname"), rs.getString("lname"), rs.getString("phoneNumber"), rs.getInt("dtcstatus"), null, null, null);
		}
		
		return user;
	}
	
	private boolean updateTryna(TrynaUpdate trynaUpdate) {
		try {
			PreparedStatement ps = conn.prepareStatement(Constants.DATABASETRYNAUPDATE);
			ps.setString(1, trynaUpdate.getTrynaStatus());
			ps.setInt(2, userID);
			ps.executeUpdate();
			
			return true;
		} catch(SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}
	
//	private void sendUpdateFriendData(FriendUpdate fu) {
//		
//	}
//	private void createDTCRequest(UpcommingPlan dtcr) {
//		
//	}
}
