package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Vector;

import constants.Constants;
import message.ChatMessage;
import message.DTCUpdate;
import message.DTCUser;
import message.DataMessage;
import message.FNameUpdate;
import message.FriendRequest;
import message.FriendRequestResponse;
import message.LNameUpdate;
import message.RejectUpcomingPlan;
import message.TrynaUpdate;
import message.UpcommingPlan;
import message.UpcommingPlanResponse;

public class ServerListener extends Thread {
	private DTCServerGUI dsg;
	private ServerSocket ss;
	private volatile boolean started;
	private Vector<ServerConnectionManager> connectionList;
	private HashMap<String, ServerConnectionManager> connectedList;
	private Connection conn;
	public ServerListener(DTCServerGUI dsg) {
		this.dsg = dsg;
		connectionList = new Vector<ServerConnectionManager>();
		connectedList = new HashMap<String, ServerConnectionManager>();
		
		try {
			Class.forName(Constants.DATABASEDRIVER);
			conn = DriverManager.getConnection(Constants.DATABASELOGIN);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		started = true;
		try {
			ss = new ServerSocket(Constants.PORT);
			dsg.sendMessage(Constants.STARTINGMESSAGE);
			
			while(started) {
				Socket s = ss.accept();
				dsg.sendMessage(Constants.CONNECTIONESSAGE + s.getInetAddress());
				connectionList.add(new ServerConnectionManager(s, dsg, this));
			}
		} catch (IOException ioe) {
			//ioe.printStackTrace();
			//shutDown();
		} finally {
			shutDown();
		}
	}
	
	void sendToThreads(DataMessage data) {
		if(data instanceof FriendRequest) {
			FriendRequest friendRequest = (FriendRequest)data;
			ServerConnectionManager scm = connectedList.get(friendRequest.getToID());
			if(scm != null) {
				scm.sendToClient(friendRequest, "FriendRequest");
			}
			try {
				PreparedStatement ps = conn.prepareStatement(Constants.DATABASEADDFRIENDREQUEST);
				ps.setString(1, friendRequest.getFromID());
				ps.setString(2, friendRequest.getToID());
				ps.executeUpdate();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		else if(data instanceof FriendRequestResponse) {
			FriendRequestResponse friendRequestResponse = (FriendRequestResponse)data;
			ServerConnectionManager scm = connectedList.get(friendRequestResponse.getFromID());
			if(scm != null) {
				scm.sendToClient(friendRequestResponse, "FriendRequestResponse");
			}
		}
		else if(data instanceof FNameUpdate) {
			FNameUpdate fNameUpdate = (FNameUpdate)data;
			// not implemented yet
		}
		else if(data instanceof LNameUpdate) {
			LNameUpdate lNameUpdate = (LNameUpdate)data;
			
		}
		else if(data instanceof TrynaUpdate) {
			TrynaUpdate trynaUpdate = (TrynaUpdate)data;
			try {
				PreparedStatement ps = conn.prepareStatement(Constants.DATABASEGETONLINEFRIENDS);
				ps.setString(1, trynaUpdate.getUsername());
				ResultSet rs = ps.executeQuery();
				while(rs.next()){
					String friend = rs.getString("username");
					ServerConnectionManager scm = connectedList.get(friend);
					if(scm != null) {
						scm.sendToClient(trynaUpdate, "TrynaUpdate");
					}
				}
			} catch(SQLException e) {
				if(Constants.DEBUGMODE) {
					e.printStackTrace();
				}
			}
		}
		else if(data instanceof UpcommingPlan) {
			UpcommingPlan upcommingPlan = (UpcommingPlan)data;
			for(String invitedUser : upcommingPlan.getSendToFriends()) {
				ServerConnectionManager scm = connectedList.get(invitedUser);
				if(scm != null) {
					scm.sendToClient(upcommingPlan, "UpcommingPlan");
				}
			}
		}
		else if(data instanceof DTCUpdate){
			DTCUpdate temp = (DTCUpdate) data;
			ServerConnectionManager scm = connectedList.get(temp.getToUsername());
			if(scm != null) {
				scm.sendToClient(temp, "DTCUpdate");
			}
		}
		else if(data instanceof RejectUpcomingPlan) {
			RejectUpcomingPlan rejectUpcomingPlan = (RejectUpcomingPlan)data;
			try {
				PreparedStatement ps = conn.prepareStatement(Constants.DATABASEGETONLINEFRIENDS);
				ps.setString(1, rejectUpcomingPlan.getUsername());
				ResultSet rs = ps.executeQuery();
				while(rs.next()){
					String friend = rs.getString("username");
					ServerConnectionManager scm = connectedList.get(friend);
					if(scm != null) {
						scm.sendToClient(rejectUpcomingPlan, "RejectUpcomingPlan");
					}
				}
			} catch(SQLException e) {
				if(Constants.DEBUGMODE) {
					e.printStackTrace();
				}
			}
			
		}
		else if(data instanceof UpcommingPlanResponse) {
			UpcommingPlanResponse upcommingPlanResponse = (UpcommingPlanResponse)data;
			for(String friends : upcommingPlanResponse.getPlan().getInviteduserNames()) {
				ServerConnectionManager scm = connectedList.get(friends);
				if(scm != null) {
					scm.sendToClient(upcommingPlanResponse, "UpcommingPlanResponse");
				}
			}
		}
		else if(data instanceof ChatMessage) {
			ChatMessage chatMessage = (ChatMessage)data;
			PreparedStatement ps;
			try {
				ps = conn.prepareStatement(Constants.DATABASEUSERBASEDONPLANONLINE);
				ps.setInt(1, chatMessage.getPlanID());
				ResultSet rs = ps.executeQuery();
				
				while(rs.next()) {
					String friend = rs.getString("username");
					ServerConnectionManager scm = connectedList.get(friend);
					if(scm != null) {
						scm.sendToClient(chatMessage, "ChatMessage");
					}
				}
			} catch (SQLException e) {
				if(Constants.DEBUGMODE) {
					e.printStackTrace();
				}
			}
			
		}
		
	}

	void shutDown() {
		if (ss != null && !ss.isClosed()) {
			for(ServerConnectionManager scm : connectionList) {
				scm.disconnect();
			}
			
			try {
				dsg.sendMessage("Shutting down");
		    	ss.close();
		    	ss = null;
		    	started = false;
			} catch (IOException e)
			{
		    	e.printStackTrace();
			}
		}
	}
	
	void putOnHashMap(String username, ServerConnectionManager scm) {
		connectedList.put(username, scm);
	}
}
