package message;

public class RejectUpcomingPlan extends DataMessage{
	private String username;
	private int planID;
	private UpcommingPlan plan;
	public RejectUpcomingPlan(int planID, String username) {
		this.planID = planID;
		this.username = username;
	}

	public int getPlanID(){
		return planID;
	}
	
	public String getUsername() {
		return username;
	}

	public UpcommingPlan getPlan() {
		return plan;
	}

	public void setPlan(UpcommingPlan plan) {
		this.plan = plan;
	}
}
