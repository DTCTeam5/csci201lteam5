package message;

public class OnlineUpdate extends DataMessage {
	private static final long serialVersionUID = -8856085273605577914L;
	private boolean onlineStatus;

	public OnlineUpdate(boolean onlineStatus) {
		this.onlineStatus = onlineStatus;
	}

	public boolean isOnlineStatus() {
		return onlineStatus;
	}
}
