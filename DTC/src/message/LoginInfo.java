package message;

import constants.Constants;

public class LoginInfo extends DataMessage {
	private static final long serialVersionUID = 7662963719354277995L;
	private String userName;
	private String encryptedPassword;
	public LoginInfo(String userName, String unencryptedPassword) {
		
		this.userName = userName;
		this.encryptedPassword = Constants.hashPass(unencryptedPassword);
	}
	
	public LoginInfo(RegisterInfo regInfo) {
		this.userName = regInfo.getUserName();
		this.encryptedPassword = regInfo.getPassword();
	}
	
	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return encryptedPassword;
	}
}
