package message;

public class DeleteFriend extends DataMessage {
	private static final long serialVersionUID = 1469382232503979229L;
	private String userName;

	public DeleteFriend(String userName) {
		this.userName = userName;
	}

	public String getUserName() {
		return userName;
	}	
}
