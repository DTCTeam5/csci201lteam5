package message;

public class EmailSearch extends DataMessage {
	private static final long serialVersionUID = -5309958780258659748L;
	private String email;
	public EmailSearch(String email) {
		this.email = email;
	}
	
	public String getEmail() {
		return email;
	}
}
