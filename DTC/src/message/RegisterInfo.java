package message;

import javax.swing.ImageIcon;

import constants.Constants;

public class RegisterInfo extends DataMessage {
	private static final long serialVersionUID = 5889232139045848510L;
	private String userName;
	private String encryptedPassword;
	private String email;
	private String fName;
	private String lName;
	private String phoneNumber;
	private ImageIcon profilePicture;
	public RegisterInfo(String fName, String lName, String email, String phoneNumber, String userName, String unencryptedPassword, ImageIcon profilePicture) {
		this.userName = userName;
		this.encryptedPassword = Constants.hashPass(unencryptedPassword);
		this.email = email;
		this.fName = fName;
		this.lName = lName;
		this.phoneNumber = phoneNumber;
		this.profilePicture = profilePicture;
	}
	
	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return encryptedPassword;
	}

	public String getEmail() {
		return email;
	}

	public String getfName() {
		return fName;
	}

	public String getlName() {
		return lName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public ImageIcon getProfilePicture(){
		return profilePicture;
	}
}
