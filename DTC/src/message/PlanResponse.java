package message;

public class PlanResponse extends DataMessage {
	private static final long serialVersionUID = 7425325345737119034L;
	private DTCUser fromUser;
	private Integer planID;
	private Boolean response;
	
	public PlanResponse(DTCUser fromUser, Integer planID, Boolean response) {
		this.fromUser = fromUser;
		this.planID = planID;
		this.response = response;
	}
	
	public DTCUser getFromUser() {
		return fromUser;
	}

	public Integer getPlanID() {
		return planID;
	}

	public Boolean getResponse() {
		return response;
	}
}
