package message;

public class LeavePlan extends DataMessage {
	private static final long serialVersionUID = -6451276420038386374L;
	private String planTitle;
	private int planID;
	private DTCUser leavingUser;
	
	public LeavePlan(String planTitle, int planID, DTCUser leavingUser) {
		this.planTitle = planTitle;
		this.planID = planID;
		this.leavingUser = leavingUser;
	}
	
	public String getPlanTitle() {
		return planTitle;
	}

	public int getPlanID() {
		return planID;
	}

	public DTCUser getLeavingUser() {
		return leavingUser;
	}
}
