package message;

public class PhoneSearch extends DataMessage {
	private static final long serialVersionUID = -1346055061038688703L;
	private String phoneNumber;
	public PhoneSearch(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
	
	public String getPhoneNumber() {
		return phoneNumber;
	}
}
