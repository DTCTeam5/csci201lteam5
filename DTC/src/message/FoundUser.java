package message;

import javax.swing.ImageIcon;

public class FoundUser extends DataMessage {
	private static final long serialVersionUID = 3659555260975353915L;
	private boolean found;
	private String username;
	private String fName;
	private String lName;
	private ImageIcon profilePicture;
	public FoundUser(boolean found, String username, String firstName, String lastName, ImageIcon profilePicture) {
		this.found = found;
		this.username = username;
		this.fName = firstName;
		this.lName = lastName;
		this.profilePicture = profilePicture;
	}
	public boolean isFound() {
		return found;
	}
	public String getUsername() {
		return username;
	}
	public String getfName() {
		return fName;
	}
	public String getlName() {
		return lName;
	}

	public ImageIcon getProfilePicture() {
		return profilePicture;
	}
}
