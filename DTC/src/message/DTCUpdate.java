package message;

public class DTCUpdate extends DataMessage {
	private static final long serialVersionUID = 1L;
	private int status;
	private String userName;
	private String toUsername;

	public DTCUpdate(int status,String username, String toUsername) {
		this.status = status;
		userName = username;
		this.toUsername = toUsername;
	}

	public int getStatus() {
		return status;
	}
	public String getUsername(){
		return userName;
	}
	public String getToUsername() {
		return toUsername;
	}
}
