package message;

public class ChatMessage extends DataMessage {
	private static final long serialVersionUID = -8263342565702776134L;
	private String username;
	private int planID;
	private String message;
	
	public ChatMessage(String username, int planID, String message) {
		this.username = username;
		this.planID = planID;
		this.message = username + ": " + message;
	}
	
	public String getUsername() {
		return username;
	}

	public int getPlanID() {
		return planID;
	}

	public String getMessage() {
		return message;
	}
}
