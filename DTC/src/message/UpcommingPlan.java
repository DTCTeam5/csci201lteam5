package message;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.Vector;

public class UpcommingPlan extends DataMessage {
	private static final long serialVersionUID = 3398638017461879948L;
	private int planID;
	private String title;
	private String description;
	private String streetAddress;
	private Timestamp time;
	private String creatorUsername;
	// this will be people that have accepted
	private Vector<String> inviteduserNames;
	// tihs will be people that we have just invited
	private Vector<String> sendToFriends;
	private Vector<String> goingFriends;
	private boolean accepted;
	
	public UpcommingPlan(String title, String description, Timestamp time, String creatorUsername,
			Vector<String> inviteduserNames, String streetAddress, Vector<String> sendToFriends) {
		this.title = title;
		this.description = description;
		this.time = time;
		this.creatorUsername = creatorUsername;
		this.inviteduserNames = inviteduserNames;
		this.streetAddress = streetAddress;
		this.sendToFriends = sendToFriends;
		
		goingFriends = new Vector<String>();
	}
	
	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public Timestamp getTime() {
		return time;
	}

	public String getCreatorUsername() {
		return creatorUsername;
	}

	public Vector<String> getInviteduserNames() {
		return inviteduserNames;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public Vector<String> getSendToFriends() {
		return sendToFriends;
	}
	
	public int getPlanID() {
		return planID;
	}
	
	public void setPlanID(int ID) {
		planID = ID;
	}
	
	public boolean getAccepted() {
		return accepted;
	}
	
	public void setAccepted(boolean b) {
		this.accepted = b;
	}
	
	public void setInviteduserNames(Vector<String> inviteduserNames) {
		this.inviteduserNames = inviteduserNames;
	}
	
	public void removeFriend(String username) {
		inviteduserNames.removeElement(username);
	}
	
	public Vector<String> getGoingFriends() {
		return goingFriends;
	}
	
	public void setGoingFriends(Vector<String> goingFriends) {
		this.goingFriends = goingFriends;
	}
	
}
