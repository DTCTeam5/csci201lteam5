package message;

import java.util.Vector;

import javax.swing.ImageIcon;

public class DTCUser extends DataMessage {
	private static final long serialVersionUID = 1L;
	private String userName;
	private String email;
	private String fName;
	private String lName;
	private String phoneNumber;
	private int DTCStatus;
	private String trynaStatus;
	private ImageIcon profilePicture;
	private Vector<DTCUser> friendsList;
	
	public DTCUser(String userName, String email, String fName, String lName, String phoneNumber,
			int DTCStatus, String trynaStatus, ImageIcon profilePicture, Vector<DTCUser> friendsList) {
		this.userName = userName;
		this.email = email;
		this.fName = fName;
		this.lName = lName;
		this.phoneNumber = phoneNumber;
		this.DTCStatus = DTCStatus;
		this.trynaStatus = trynaStatus;
		this.profilePicture = profilePicture;
		this.friendsList = friendsList;
	}

	public String getUserName() {
		return userName;
	}

	public String getEmail() {
		return email;
	}

	public String getfName() {
		return fName;
	}

	public String getlName() {
		return lName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public int getDTCStatus() {
		return DTCStatus;
	}

	public String getTrynaStatus() {
		return trynaStatus;
	}

	public ImageIcon getProfilePicture() {
		return profilePicture;
	}

	public Vector<DTCUser> getFriendsList() {
		return friendsList;
	}
	
	public void addFriend(DTCUser friend) {
		friendsList.add(friend);
	}
}
