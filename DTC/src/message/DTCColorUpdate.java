package message;

public class DTCColorUpdate extends DataMessage {

	private static final long serialVersionUID = 2472506438736884790L;

	private int color;
	private String username;
	private String toUsername;
	public  DTCColorUpdate(int color){
		this.color = color;
		
		
	}
	public String getToUsername(){
		return toUsername;
	}
	public void setUsername(String username){
		this.username = username;
	}
	public void setToUsername(String toUsername){
		this.toUsername = toUsername;
	}
	public String getUsername(){
		return username;
	}
	public void setDTCColor(int color){
		this.color = color;
	}
	public int getDTCColor(){
		return color;
	}
}
