package message;

public class UsernameSearch extends DataMessage {
	private static final long serialVersionUID = -986160311236031830L;
	private String username;
	public UsernameSearch(String username) {
		this.username = username;
	}
	
	public String getUsername() {
		return username;
	}
}
