package message;

public class FNameUpdate extends DataMessage {
	private static final long serialVersionUID = 2253086796330986977L;
	private String fname;
	public FNameUpdate(String fname) {
		this.fname = fname;
	}
	
	public String getFname() {
		return fname;
	}
	
}
