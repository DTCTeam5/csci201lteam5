package message;

public class TrynaUpdate extends DataMessage {
	private static final long serialVersionUID = -1992689099791728238L;
	private String trynaStatus;
	private String username;
	public TrynaUpdate(String username, String trynaStatus) {
		this.username = username;
		this.trynaStatus = trynaStatus;
	}

	public String getUsername() {
		return username;
	}
	public String getTrynaStatus() {
		return trynaStatus;
	}
}
