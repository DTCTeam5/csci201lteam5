package message;

public class PasswordUpdate extends DataMessage {
	private static final long serialVersionUID = 2105174898532539445L;
	private String password;
	public PasswordUpdate(String password) {
		this.password = password;
	}
	
	public String getPassword() {
		return password;
	}
}
