package message;

public class FriendRequestResponse extends DataMessage {
	private static final long serialVersionUID = 1198147225296296289L;
	private String fromID;
	private String toID;
	private boolean response;
	private DTCUser user;
	
	public FriendRequestResponse(FriendRequest friendRequest, boolean response) {
		this.fromID = friendRequest.getFromID();
		this.toID = friendRequest.getToID();
		this.response = response;
	}
	public FriendRequestResponse(String fromID, String toID, boolean response) {
		this.fromID = fromID;
		this.toID = toID;
		this.response = response;
	}
	
	public String getFromID() {
		return fromID;
	}
	public String getToID() {
		return toID;
	}
	public boolean getResponse() {
		return response;
	}
	public DTCUser getUser() {
		return user;
	}
	public void setUser(DTCUser user) {
		this.user = user;
	}
}
