package message;

import javax.swing.ImageIcon;

public class PictureUpdate extends DataMessage {
	private static final long serialVersionUID = -3726490095020393726L;
	private ImageIcon profilePicture;
	public PictureUpdate(ImageIcon profilePicture) {
		this.profilePicture = profilePicture;
	}

	public ImageIcon getProfilePicture() {
		return profilePicture;
	}
}
