package message;

public class LNameUpdate extends DataMessage {
	private static final long serialVersionUID = 8080042753871349576L;
	private String lname;
	public LNameUpdate(String lname) {
		this.lname = lname;
	}
	
	public String getLname() {
		return lname;
	}
	
}
