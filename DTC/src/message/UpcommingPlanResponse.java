package message;

public class UpcommingPlanResponse extends DataMessage {
	private static final long serialVersionUID = 1583605605956632151L;
	private int planID;
	private boolean response;
	private UpcommingPlan plan;
	public UpcommingPlanResponse(int planID, boolean response) {
		this.planID = planID;
		this.response = response;
	}
	
	public int getPlanID() {
		return planID;
	}
	public boolean getResponse() {
		return response;
	}
	public UpcommingPlan getPlan() {
		return plan;
	}
	public void setPlan(UpcommingPlan plan) {
		this.plan = plan;
	}
}
