package message;

public class ForgotInfo extends DataMessage {
	private static final long serialVersionUID = -9028159836434315371L;
	private String email;
	public ForgotInfo(String email) {
		this.email = email;
	}
	
	public String getEmail() {
		return email;
	}

}
