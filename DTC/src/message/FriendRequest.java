package message;

public class FriendRequest extends DataMessage {
	private static final long serialVersionUID = -92305941922399512L;
	private String fromID;
	private String toID;
	
	public FriendRequest(String fromID, String toID) {
		this.fromID = fromID;
		this.toID = toID;
	}

	public String getFromID() {
		return fromID;
	}

	public String getToID() {
		return toID;
	}
}
