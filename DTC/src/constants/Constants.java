package constants;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.swing.JButton;

public class Constants {
	public static final double VERSION = 0.1;
	public static final int PORT = 6789;
	public static final String HOSTNAME = "localhost";
	public static final boolean DEBUGMODE = true;
	
	public static final String STARTINGMESSAGE = "Server started on port " + PORT + ".";
	public static final String CONNECTIONESSAGE = "Connection from ";
	
	public static final int STATUSOFFLINE = 0;
	public static final int STATUSONLINE = 1;
	public static final int STATUSAWAY = 2;
	
	public static final int STATEIDLE = 100;
	public static final int STATEUNREGISTERED = 101;
	public static final int STATEREGISTERED = 102;
	
	public static final String DATABASEUSER = "root";
	public static final String DATABASEDRIVER = "com.mysql.jdbc.Driver";
	public static final String DATABASESTATUS = "UPDATE Users SET DTCStatus = ? WHERE Users.username = ?";
	public static final String DATABASELOGIN = "jdbc:mysql://localhost/DTCDatabase?useSSL=false&user=root&password=";
	public static final String DATABASEGETUSERPASS = "SELECT * FROM Users WHERE username=? AND pass=?;";
	//public static final String DATABASEGETUSER = "SELECT * FROM Users WHERE username=?";
	public static final String DATABASEGETUSER = "SELECT Users.username, Users.email, Users.fname, Users.lname, Users.phoneNumber, Users.dtcstatus, Users.trynastatus FROM Users WHERE username=?;";
	public static final String DATABASEGETPHONE = "SELECT Users.username, Users.fname, Users.lname FROM Users WHERE phoneNumber=?";
	public static final String DATABASEGETEMAIL = "SELECT Users.username, Users.fname, Users.lname FROM Users WHERE email=?";
	public static final String DATABASEREGISTER = "INSERT INTO Users ( username, pass, email, fname, lname, phoneNumber) VALUES (?, ?, ?, ?, ?, ?);";
	public static final String DATABASEGETFRIENDS = "SELECT FriendsList.friendID, Users.username, Users.email, Users.fname, Users.lname, Users.phoneNumber, Users.dtcstatus, Users.trynastatus FROM FriendsList INNER JOIN Users ON FriendsList.toID=Users.userID WHERE FriendsList.fromID=?;";
	public static final String DATABASEADDFRIEND = "INSERT INTO FriendsList (fromID, toID) VALUE ((SELECT Users.userID from Users WHERE username = ?), (SELECT Users.userID from Users WHERE username = ?));";
	public static final String DATABASEISFRIEND = "SELECT FriendsList.pending FROM FriendsList, Users WHERE FriendsList.fromID=? AND FriendsList.toID=(Select Users.username FROM Users WHERE Users.username=?);";
	public static final String DATABASEUPDATEFNAME = "UPDATE Users SET fname = ? WHERE userID = ?";
	public static final String DATABASEUPDATELNAME = "UPDATE Users SET lname = ? WHERE userID = ?";
	public static final String DATABASEUPDATEPASSWORD = "UPDATE Users SET pass = ? WHERE userID = ?";
	public static final String DATABASETRYNAUPDATE = "UPDATE Users SET trynastatus=? WHERE userID=?;";
	
	public static final String DATABASEGETONLINEFRIENDS = "SELECT  Users.username,Users.dtcstatus FROM Users INNER JOIN FriendsList ON FriendsList.toID=Users.userID  WHERE FriendsList.fromID=(SELECT Users.userID FROM Users WHERE username=?) AND dtcstatus>=1;";
	
	public static final String DATABASEGETFRIENDREQUEST = "SELECT Users.username FROM FriendRequests INNER JOIN Users ON FriendRequests.fromUserID=Users.userID WHERE FriendRequests.toUserID=(SELECT Users.userID FROM Users WHERE Users.username=?);";
	public static final String DATABASEADDFRIENDREQUEST = "INSERT FriendRequests (fromUserID, toUserID) VALUE ((SELECT Users.userID FROM Users WHERE Users.username=?), (SELECT Users.userID FROM Users WHERE Users.username=?));";
	public static final String DATABASEREMOVEFRIENDREQUEST = "DELETE FROM FriendRequests WHERE FriendRequests.fromUserID=(SELECT Users.userID FROM Users WHERE Users.username=?) AND FriendRequests.toUserID=(SELECT Users.userID FROM Users WHERE Users.username=?)";
	
	public static final String DATABASECREATEPLAN = "INSERT INTO Plan (creatorID, plantime, title, description, address) VALUE ((SELECT Users.userID FROM Users WHERE Users.username=?), ?, ?, ?, ?);";
	public static final String DATABASEGETPLANID = "SELECT Plan.planID FROM Plan WHERE Plan.planTime=? AND Plan.title=?";
	public static final String DATABASEADDPLANREQUEST = "INSERT PlanUsers (planID, userID, accepted, creator) VALUE (?, (SELECT Users.userID FROM Users WHERE Users.username=?), ?, ?);";
	public static final String DATABASEGETUSERPLANS = "SELECT Plan.planID, Plan.plantime, Plan.title, Plan.description, PlanUsers.accepted, Plan.plantime, Users.username, Plan.address FROM Plan INNER JOIN Users ON Plan.creatorID=Users.userID INNER JOIN PlanUsers ON Plan.planID=PlanUsers.planID WHERE PlanUsers.userID=(SELECT Users.userID FROM Users WHERE Users.username=?);";
	public static final String DATABASEGETUSERSINVOLVEDINPLAN = "SELECT Users.username, PlanUsers.accepted FROM PlanUsers INNER JOIN Users ON PlanUsers.userID=Users.userID WHERE PlanID=?;";
	public static final String DTABASEACCEPTPLAN = "UPDATE PlanUsers SET PlanUsers.accepted=1 WHERE PlanUsers.userID=(SELECT Users.userID FROM Users WHERE Users.username=?) AND planID=?;";
	public static final String DATABASEREJECTPLAN = "DELETE FROM PlanUsers WHERE PlanUsers.planID=? AND PlanUsers.userID=(SELECT Users.userID FROM Users WHERE Users.username=?);";
	public static final String DATABASELEAVEPLAN = "DELETE FROM PlanUsers WHERE PlanUsers.planID=? AND PlanUsers.userID=(SELECT Users.userID FROM Users WHERE Users.username=?)";
	public static final String DATABASEGETPLAN = "SELECT Plan.title, Plan.description, Plan.plantime, Users.username, Plan.address FROM Plan INNER JOIN Users ON Users.userID=Plan.creatorID WHERE Plan.planID=?;";
	
	public static final String DATABASEUSERBASEDONPLANONLINE = "SELECT Users.username FROM PlanUsers INNER JOIN Users ON PlanUsers.userID=Users.userID WHERE PlanUsers.planID=? AND Users.DTCStatus>0;";
	public static String hashPass(String unencryptedPassword) {
		MessageDigest md;
		StringBuffer stringBuffer = null;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(unencryptedPassword.getBytes());
			byte[] digest = md.digest();
			stringBuffer = new StringBuffer();
			for (byte bytes : digest) {
                stringBuffer.append(String.format("%02x", bytes & 0xff));
            }
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if(stringBuffer == null)
			return null;
		return stringBuffer.toString();
	}
}
